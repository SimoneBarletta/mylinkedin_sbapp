package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.SavingUserException;
import it.unisalento.sbapp.dao.AuthorityRepository;
import it.unisalento.sbapp.dao.UserRepository;
import it.unisalento.sbapp.domain.Applicant;
import it.unisalento.sbapp.domain.Authority;
import it.unisalento.sbapp.domain.AuthorityName;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.iservice.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
class UserServiceImplTest {

    @Mock
    private User fakeUser;

    @Autowired
    private IUserService userService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private AuthorityRepository authorityRepository;

    @Test
    void save() throws SavingUserException {
        Applicant user = Applicant.builder().password("123456").build();
        User userSaved = userService.save(user);
        assertTrue(userSaved.getAuthorities().stream().map(Authority::getName).anyMatch(name -> name.equals(AuthorityName.ROLE_APPLICANT)));
    }

    @Test
    void saveFail() throws SavingUserException {
        User userSaved = userService.save(fakeUser);
        assertFalse(userSaved.getAuthorities().stream().map(Authority::getName).anyMatch(name -> name.equals(AuthorityName.ROLE_APPLICANT)));
    }

    @BeforeEach
    void setUp() {
        Mockito.when(fakeUser.getType()).thenReturn("fakerole");
        Mockito.when(fakeUser.getPassword()).thenReturn("fake");
        Mockito.when(authorityRepository.save(any(Authority.class))).thenAnswer(authority ->
                authority.getArguments()[0]);
        Mockito.when(userRepository.save(any(User.class))).thenAnswer(user ->
                user.getArguments()[0]);
    }

}