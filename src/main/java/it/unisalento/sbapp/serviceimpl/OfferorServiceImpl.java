package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.dao.OfferorRepository;
import it.unisalento.sbapp.domain.Offeror;
import it.unisalento.sbapp.iservice.IOfferorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class OfferorServiceImpl implements IOfferorService {
    @Autowired
    OfferorRepository offerRepo;

    @Transactional
    public List<Offeror> getAll(){
        return offerRepo.findAll();
    }
}
