package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.AttributeNotFoundException;
import it.unisalento.sbapp.dao.AttributeRepository;
import it.unisalento.sbapp.domain.Attribute;
import it.unisalento.sbapp.iservice.IAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AttributeServiceImpl implements IAttributeService {

    @Autowired
    AttributeRepository attributeRepository;

    @Override
    @Transactional(rollbackOn = AttributeNotFoundException.class)
    public Attribute getAttributeById(int id) throws AttributeNotFoundException {
        return attributeRepository.findById(id).orElseThrow(() -> new AttributeNotFoundException());
    }

    @Override
    @Transactional(rollbackOn = AttributeNotFoundException.class)
    public Attribute getAttributeByName(String name) throws AttributeNotFoundException {
        return attributeRepository.getAttributeByName(name);
    }

    @Override
    @Transactional(rollbackOn = AttributeNotFoundException.class)
    public List<Attribute> getByType(String type) throws AttributeNotFoundException {
        return attributeRepository.getByType(type);
    }

    @Override
    @Transactional(rollbackOn = AttributeNotFoundException.class)
    public List<Attribute> getAll() throws AttributeNotFoundException {
        return attributeRepository.findAll();
    }

    @Override
    public void save(Attribute attribute) {
        attributeRepository.save(attribute);
    }

    @Override
    @Transactional(rollbackOn = AttributeNotFoundException.class)
    public void delete(int id) throws AttributeNotFoundException{
        attributeRepository.deleteById(id);
    }

    @Override
    public int getAttributeId(Attribute attribute){
        return attribute.getId();
    }

    @Override
    @Transactional
    public void update(Attribute attribute) {
        attributeRepository.update(attribute.getType(), attribute.getDescription(), attribute.getId());
    }
}
