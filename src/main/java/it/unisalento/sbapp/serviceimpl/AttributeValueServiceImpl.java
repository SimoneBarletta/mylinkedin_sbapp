package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.AttributeStructureNotFoundException;
import it.unisalento.sbapp.Exceptions.AttributeValueNotFoundException;
import it.unisalento.sbapp.dao.AttributeValueRepository;
import it.unisalento.sbapp.domain.Attribute;
import it.unisalento.sbapp.domain.AttributeValue;
import it.unisalento.sbapp.iservice.IAttributeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AttributeValueServiceImpl implements IAttributeValueService {

    @Autowired
    AttributeValueRepository attributeValueRepository;

    @Override
    @Transactional(rollbackOn = AttributeValueNotFoundException.class)
    public AttributeValue getById(int id) throws AttributeValueNotFoundException {
        return attributeValueRepository.findById(id).orElseThrow(()-> new AttributeValueNotFoundException());
    }

    @Override
    @Transactional(rollbackOn = AttributeValueNotFoundException.class)
    public List<AttributeValue> getAll() throws AttributeValueNotFoundException {
        return attributeValueRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = AttributeValueNotFoundException.class)
    public List<AttributeValue> getAllByPostId(int idPost) throws AttributeValueNotFoundException {
        return attributeValueRepository.getAllByPostId(idPost);
    }

    @Override
    @Transactional(rollbackOn = AttributeValueNotFoundException.class)
    public List<AttributeValue> getAllByAttributeId(int idAttribute) throws AttributeValueNotFoundException {
        return attributeValueRepository.getAllByAttributeId(idAttribute);
    }

    @Override
    @Transactional(rollbackOn = AttributeValueNotFoundException.class)
    public void save(AttributeValue attributeValue) {
        attributeValueRepository.save(attributeValue);
    }

    @Override
    @Transactional(rollbackOn = AttributeValueNotFoundException.class)
    public int getAttributeValueId(AttributeValue attributeValue) {
        return attributeValue.getId();
    }

    @Override
    @Transactional(rollbackOn = AttributeValueNotFoundException.class)
    public void delete(int id) {
        attributeValueRepository.deleteById(id);
    }

    @Override
    @Transactional(rollbackOn = AttributeValueNotFoundException.class)
    public List<AttributeValue> findAttributeByName(String attribute_name, int postId) {
        return attributeValueRepository.findAttributeByName(attribute_name, postId);
    }
}
