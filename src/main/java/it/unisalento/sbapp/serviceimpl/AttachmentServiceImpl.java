package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.*;
import it.unisalento.sbapp.dao.AttachmentRepository;
import it.unisalento.sbapp.dao.AttributeRepository;
import it.unisalento.sbapp.dao.UserRepository;
import it.unisalento.sbapp.domain.Attachment;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.iservice.IAttachmentService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AttachmentServiceImpl implements IAttachmentService {

    @Autowired
    UserRepository userRepo;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttributeRepository attributeRepository;

    @Override
    @Transactional
    public Attachment getById(int id) {
        return attachmentRepository.getOne(id);
    }

    @Override
    @Transactional
    public int getAttachmentId(Attachment attachment) {
        return attachment.getId();
    }

    @Override
    @Transactional(rollbackOn = UserNotFoundExeception.class)
    public List<Attachment> getAttachmentByUserId(int userId) throws UserNotFoundExeception {
        User user = userRepo.findById(userId).orElseThrow(()-> new UserNotFoundExeception());
        return attachmentRepository.findByUser(user);
    }

    @Override
    @Transactional(rollbackOn = PostNotFoundException.class)
    public List<Attachment> getAttachmentByPostId(int postId) throws PostNotFoundException {
        try {
            return attachmentRepository.findByPostId(postId);
        } catch (Exception e) {
            // TODO: handle exception
            throw new PostNotFoundException();
        }
    }

    @Transactional(rollbackOn = SavingUserException.class)
    public Attachment save(Attachment attachment) throws SavingUserException{
        try {
            return attachmentRepository.save(attachment);
        } catch (Exception e) {
            // TODO: handle exception
            throw new SavingUserException();
        }

    }

    @Override
    @SneakyThrows
    @Transactional(rollbackOn = AttachmentNotFoundException.class)
    public void delete(int id) throws AttachmentNotFoundException{
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(()-> new AttachmentNotFoundException());
        attachmentRepository.delete(attachment);
    }

    @Override
    @SneakyThrows
    @Transactional(rollbackOn = AttachmentNotFoundException.class)
    public List<Attachment> getAll() {
        return this.attachmentRepository.findAll();
    }
}
