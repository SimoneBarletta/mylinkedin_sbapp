package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.ProfileImageNotFoundException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.dao.ProfileImageRepository;
import it.unisalento.sbapp.domain.ProfileImage;
import it.unisalento.sbapp.iservice.IProfileImageService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ProfileImageImpl implements IProfileImageService {

    @Autowired
    ProfileImageRepository profileImageRepository;

    @SneakyThrows
    @Transactional(rollbackOn = ProfileImageNotFoundException.class)
    public ProfileImage getById(int id) throws ProfileImageNotFoundException{
        return profileImageRepository.findById(id).orElseThrow(()->new ProfileImageNotFoundException());
    }

    @Override
    @Transactional
    public int getProfileImageId(ProfileImage profileImage)   {
        return profileImage.getId();
    }

    @Override
    @Transactional
    public ProfileImage save(ProfileImage profileImage){
        return profileImageRepository.save(profileImage);
    }

    @Override
    @Transactional
    public ProfileImage getByUserId(Integer userId) throws UserNotFoundExeception {
        return profileImageRepository.getByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteByUserId(Integer idUser) throws ProfileImageNotFoundException, UserNotFoundExeception{
            profileImageRepository.deleteByUserId(idUser);
    }


}
