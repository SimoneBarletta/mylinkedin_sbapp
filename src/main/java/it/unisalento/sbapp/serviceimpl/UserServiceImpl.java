package it.unisalento.sbapp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import it.unisalento.sbapp.dao.*;
import it.unisalento.sbapp.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.Exceptions.SavingUserException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.iservice.IUserService;

@Service
public class UserServiceImpl implements IUserService {
	
	@Autowired
	protected
	UserRepository userRepo;

	@Autowired
	protected
	PostRepository postRepository;

	@Autowired
	AuthorityRepository authorityRepository;

	@Autowired
	CommentRepository commentRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	AttachmentRepository attachmentRepository;

	@Autowired
	Matching_JobApplicationRepository matching_jobApplicationRepository;
	
	@Transactional(rollbackOn = SavingUserException.class)
	public User save(User user) throws SavingUserException{

		Authority authority=new Authority();
		List<Authority> authorities = new ArrayList<>();

		switch (user.getType()){
			case "admin":
				authority.setName(AuthorityName.ROLE_ADMIN);
				authority=authorityRepository.save(authority);
				authorities.add(authority);
				break;
			case "offeror":
				authority.setName(AuthorityName.ROLE_OFFEROR);
				authority=authorityRepository.save(authority);
				authorities.add(authority);
				break;
			case "applicant":
				authority.setName(AuthorityName.ROLE_APPLICANT);
				authority=authorityRepository.save(authority);
				authorities.add(authority);
				break;
		}
		try {
			user.setAuthorities(authorities);
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			user.setEnabled(false);
			return userRepo.save(user);
		} catch (Exception e) {
			// TODO: handle exception
			throw new SavingUserException();
		}
		
	}
	
	
	@Transactional(rollbackOn = UserNotFoundExeception.class)
	public User getById(Integer id) throws UserNotFoundExeception{
		
		return userRepo.findById(id).orElseThrow(()->new UserNotFoundExeception());
		
	}

	@Override
	@Transactional
	public int getUserId(User user) {
		return user.getId();
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundExeception.class)
	public User getByEmail(String email) throws UserNotFoundExeception {
		return userRepo.findByEmail(email);
	}

	@Override
	@Transactional
	public List<User> getByType(String type) {
		return userRepo.findByType(type);
	}


	@Override
	@Transactional
	public void banUser(int id) throws UserNotFoundExeception {
		userRepo.banById(id);
		postRepository.hidePostBannedUser(id);
		matching_jobApplicationRepository.deleteAllByUserId(id);
	}

	@Transactional
	@Override
	public void unbanUser(int id) throws UserNotFoundExeception {
		userRepo.unbanById(id);
		postRepository.showPostUnBannedUser(id);
	}


	@Transactional
	public List<User> getAll() {
		return userRepo.findAll();
	}
	
	@Transactional(rollbackOn = {UserNotFoundExeception.class, EntityNotFoundException.class})
	public void delete(int id) throws UserNotFoundExeception {
		User user = userRepo.findById(id).orElseThrow(()->new UserNotFoundExeception());
		commentRepository.deleteByUserId(user.getId());
		attachmentRepository.deleteByUserId(user.getId());
		userRepo.delete(user);
	}



	@Override
	@Transactional
	public List<User> getPendingUsers() {
		return userRepo.findPendingUsers();
	}


	@Override
	@Transactional
	public void acceptUser(Integer id) {
			userRepo.acceptUser(id);
	}

}
