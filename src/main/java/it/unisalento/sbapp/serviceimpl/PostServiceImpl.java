package it.unisalento.sbapp.serviceimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.Exceptions.PostNotFoundException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.dao.PostRepository;
import it.unisalento.sbapp.dao.UserRepository;
import it.unisalento.sbapp.domain.Post;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.iservice.IPostService;

@Service
public class PostServiceImpl implements IPostService {
	
	@Autowired
	PostRepository postRepo;
	
	@Autowired
	UserRepository userRepo;
	

	@Override
	@Transactional
	public Post save(Post post) {

		return postRepo.save(post);
	}

	@Override
	@Transactional
	public int getPostId(Post post) {
		return post.getId();
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public Post getById(int id) throws PostNotFoundException {

		return postRepo.findById(id).orElseThrow(()-> new PostNotFoundException());
	}

	@Override
	@Transactional
	public List<Post> getAll() {

		return postRepo.findAll();
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public void deleteById(int id) throws PostNotFoundException {

		Post post = postRepo.findById(id).orElseThrow(()->new PostNotFoundException());
		postRepo.delete(post);

	}

	@Override
	@Transactional(rollbackOn = UserNotFoundExeception.class)
	public List<Post> getPostByUserId(int userId) throws UserNotFoundExeception {
		User user = userRepo.findById(userId).orElseThrow(()-> new UserNotFoundExeception());
		return postRepo.findByUser(user);
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public void hidePost(Integer id) {
		postRepo.hidePost(id);
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public void showPost(Integer id) {
		postRepo.showPost(id);
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundExeception.class)
	public List<Post> findByUserId(int userId) throws UserNotFoundExeception {
		return postRepo.findByUserId(userId);
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundExeception.class)
	public List<Post> findPost(String type, int userId) {
		return this.postRepo.findPost(type, userId);
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundExeception.class)
	public List<Post> findPostBySkill(String skill) {
		return this.postRepo.findPostBySkill(skill);
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundExeception.class)
	public List<Post> findPostByDate(String date) {
		return this.postRepo.findPostByDate(date);
	}

}
