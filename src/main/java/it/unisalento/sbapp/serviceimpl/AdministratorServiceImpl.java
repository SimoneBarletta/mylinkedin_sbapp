package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.dao.AdministratorRepository;
import it.unisalento.sbapp.domain.Administrator;
import it.unisalento.sbapp.iservice.IAdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AdministratorServiceImpl implements IAdministratorService {

    @Autowired
    AdministratorRepository administratorRepository;

    @Transactional
    @Override
    public List<Administrator> getAll(){
        return administratorRepository.findAll();
    }
}
