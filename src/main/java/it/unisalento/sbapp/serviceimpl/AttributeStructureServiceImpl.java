package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.AttributeStructureNotFoundException;
import it.unisalento.sbapp.dao.AttributeStructureRepository;
import it.unisalento.sbapp.domain.AttributeStructure;
import it.unisalento.sbapp.iservice.IAttributeStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AttributeStructureServiceImpl implements IAttributeStructureService{

    @Autowired
    AttributeStructureRepository attributeStructureRepository;


    @Override
    @Transactional(rollbackOn = AttributeStructureNotFoundException.class)
    public List<AttributeStructure> getAll() throws AttributeStructureNotFoundException {
        return attributeStructureRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = AttributeStructureNotFoundException.class)
    public List<AttributeStructure> getAllByStructureId(int idStructure) throws AttributeStructureNotFoundException{
        return attributeStructureRepository.getAllByStructureId(idStructure);
    }

    @Override
    @Transactional(rollbackOn = AttributeStructureNotFoundException.class)
    public List<AttributeStructure> getAllByAttributeId(int idAttribute) throws AttributeStructureNotFoundException{
        return attributeStructureRepository.getAllByAttributeId(idAttribute);
    }

    @Override
    public void save(AttributeStructure attributeStructure) {
        attributeStructureRepository.save(attributeStructure);
    }

    @Override
    public int getAttributeStructureId(AttributeStructure attributeStructure) {
        return attributeStructure.getId();
    }

    @Override
    @Transactional(rollbackOn = AttributeStructureNotFoundException.class)
    public AttributeStructure getById(int id) throws AttributeStructureNotFoundException{
        return attributeStructureRepository.findById(id).orElseThrow(()-> new AttributeStructureNotFoundException());
    }

    @Override
    @Transactional(rollbackOn = AttributeStructureNotFoundException.class)
    public void delete(int id){
        attributeStructureRepository.deleteById(id);
    }
}
