package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.Matching_JobApplicationNotFoundException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.dao.Matching_JobApplicationRepository;
import it.unisalento.sbapp.domain.Matching_JobApplication;
import it.unisalento.sbapp.iservice.IMatching_JobApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class Matching_JobApplicationServiceImpl implements IMatching_JobApplicationService {

    @Autowired
    Matching_JobApplicationRepository matching_jobApplicationRepository;

    @Override
    public void save(Matching_JobApplication matching_jobApplication) {
        matching_jobApplicationRepository.save(matching_jobApplication);
    }

    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    @Override
    public Matching_JobApplication getById(int id) throws Matching_JobApplicationNotFoundException {
        return matching_jobApplicationRepository.getOne(id);
    }

    @Override
    @Transactional (rollbackOn = Matching_JobApplicationNotFoundException.class)
    public int getMatching_JobApplicationId(Matching_JobApplication matching_jobApplication) throws Matching_JobApplicationNotFoundException {
        return matching_jobApplication.getId();
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public List<Matching_JobApplication> getAll() throws Matching_JobApplicationNotFoundException {
       return matching_jobApplicationRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public List<Matching_JobApplication> getByUserId(int userId) throws Matching_JobApplicationNotFoundException{
        return matching_jobApplicationRepository.getByUserId(userId);
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public List<Matching_JobApplication> getByPostId(int postId) throws Matching_JobApplicationNotFoundException {
        return matching_jobApplicationRepository.getByPostId(postId);
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public List<Matching_JobApplication> getByStatusAndPostId(Boolean status, int postId) throws Matching_JobApplicationNotFoundException {
        return matching_jobApplicationRepository.getByStatusAndPostId(status, postId);
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public List<Matching_JobApplication> getByStatusAndUserId(Boolean status, int userId) throws Matching_JobApplicationNotFoundException {
        return matching_jobApplicationRepository.getByStatusAndUserId(status, userId);
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public List<Matching_JobApplication> getByApplierId(int applierId) throws Matching_JobApplicationNotFoundException{
        return matching_jobApplicationRepository.getByApplierId(applierId);
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public List<Matching_JobApplication> getByStatusAndApplierId(Boolean status, int applierId) throws Matching_JobApplicationNotFoundException {
        return matching_jobApplicationRepository.getByStatusAndApplierId(status, applierId);
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public List<Matching_JobApplication> getByApplierIdOrUserId(int applierId, int userId) throws Matching_JobApplicationNotFoundException {
        return matching_jobApplicationRepository.getByApplierIdOrUserId(applierId, userId);
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public List<Matching_JobApplication> getByApplierIdOrUserIdAndStatus(int applierId, int userId, Boolean status) throws Matching_JobApplicationNotFoundException {
        return matching_jobApplicationRepository.getByApplierIdOrUserIdAndStatus(applierId, userId, status);
    }

    @Override
    @Transactional
    public void updateStatusTrue(int matchingJobId) {
        matching_jobApplicationRepository.updateStatusTrue(matchingJobId);
    }

    @Override
    @Transactional
    public void updateStatusFalse(int matchingJobId) {
        matching_jobApplicationRepository.updateStatusFalse(matchingJobId);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(int userId) {
        matching_jobApplicationRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional(rollbackOn = Matching_JobApplicationNotFoundException.class)
    public void delete(int id) throws  Matching_JobApplicationNotFoundException{
        matching_jobApplicationRepository.deleteById(id);
    }
}
