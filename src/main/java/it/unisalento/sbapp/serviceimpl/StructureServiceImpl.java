package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.StructureNotFoundException;
import it.unisalento.sbapp.dao.StructureRepository;
import it.unisalento.sbapp.domain.Structure;
import it.unisalento.sbapp.iservice.IStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
public class StructureServiceImpl implements IStructureService {

    @Autowired
    protected
    StructureRepository structureRepository;

    @Override
    @Transactional(rollbackOn = StructureNotFoundException.class)
    public Structure getById(int id) throws StructureNotFoundException{
        return structureRepository.getOne(id);
    }

    @Override
    public void save(Structure structure) {
        structureRepository.save(structure);
    }

    @Override
    public void delete(int id) {
        structureRepository.deleteById(id);
    }

    @Override
    @Transactional
    public int getStructureId(Structure structure) {
        return structure.getId();
    }

    @Override
    @Transactional(rollbackOn = StructureNotFoundException.class)
    public List<Structure> getAllStructures() throws StructureNotFoundException {
        return structureRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = StructureNotFoundException.class)
    public Structure getByTitle(String title) throws StructureNotFoundException{
        return structureRepository.getByTitle(title);
    }
}
