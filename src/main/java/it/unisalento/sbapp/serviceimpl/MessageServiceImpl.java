package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.MessageNotFoundException;
import it.unisalento.sbapp.dao.MessageRepository;
import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.domain.Sent_Message;
import it.unisalento.sbapp.iservice.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class MessageServiceImpl implements IMessageService {

    @Autowired
    MessageRepository messageRepository;

    @Override
    public int getMessageId(Message message) {
        return message.getId();
    }

    @Override
    @Transactional
    public Message save(Message message) {
        Message saved_message = messageRepository.save(message);
        return saved_message;
    }

    @Override
    @Transactional
    public void delete(int id) throws MessageNotFoundException{
    messageRepository.deleteById(id);
    }

    @Override
    public Message getMessageById(int id) throws MessageNotFoundException {
        return messageRepository.getOne(id);
    }

    @Override
    public List<Message> filterMessagesByType(List<Sent_Message> sent_messageList, String type) {
        List<Message> messages = new ArrayList<Message>();

        for (Sent_Message sent_message : sent_messageList) {
            Message message = sent_message.getMessage();
            if (message.getType().equals(type)) {
                messages.add(message);
            }
        }
        return messages;
    }
}
