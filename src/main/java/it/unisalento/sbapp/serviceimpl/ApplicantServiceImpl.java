package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.dao.ApplicantRepository;
import it.unisalento.sbapp.dao.OfferorRepository;
import it.unisalento.sbapp.domain.Applicant;
import it.unisalento.sbapp.domain.Offeror;
import it.unisalento.sbapp.iservice.IApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ApplicantServiceImpl implements IApplicantService {

    @Autowired
    ApplicantRepository applicantRepo;

    @Transactional
    public List<Applicant> getAll(){
        return applicantRepo.findAll();
    }
}
