package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.Exceptions.CommentNotFoundException;
import it.unisalento.sbapp.dao.CommentRepository;
import it.unisalento.sbapp.domain.Comment;
import it.unisalento.sbapp.dto.CommentDTO;
import it.unisalento.sbapp.iservice.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CommentServiceImpl implements ICommentService {

    @Autowired
    CommentRepository commentRepository;

    @Override
    @Transactional
    public void save(Comment comment) {
        commentRepository.save(comment);

    }

    @Override
    @Transactional
    public void delete(Integer id) throws CommentNotFoundException {
        commentRepository.deleteById(id);

    }

    @Override
    @Transactional
    public void deleteAllByParent(Integer parentId) throws CommentNotFoundException {
        commentRepository.deleteByParentId(parentId);

    }

    @Override
    @Transactional
    public void deleteAllByPost(Integer postId) throws CommentNotFoundException {
        commentRepository.deleteByPostId(postId);

    }

    @Override
    @Transactional
    public Comment getCommentById(Integer id) throws CommentNotFoundException {
        return commentRepository.findById(id).orElseThrow(()-> new CommentNotFoundException());
    }

    @Override
    @Transactional
    public List<Comment> getByPostId(Integer postId) throws CommentNotFoundException {
        return commentRepository.getByPostId(postId);
    }

    @Override
    @Transactional
    public List<Comment> getByPostIdAndParentId(Integer postId, Integer parentId) throws CommentNotFoundException {
        return commentRepository.getByPostIdAndParentId(postId, parentId);
    }

    @Override
    @Transactional
    public List<Comment> getChildrenByParentId(Integer parentId) throws CommentNotFoundException {
        return commentRepository.getChildrenByParentId(parentId);
    }

    @Override
    @Transactional
    public int getCommentId(CommentDTO commentDTO) {
        return commentDTO.getId();
    }
}
