package it.unisalento.sbapp.serviceimpl;

import it.unisalento.sbapp.dao.Sent_MessageRepository;
import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.domain.Sent_Message;
import it.unisalento.sbapp.iservice.ISent_MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class Sent_MessageServiceImpl implements ISent_MessageService {

    @Autowired
    Sent_MessageRepository sent_messageRepository;

    @Transactional
    @Override
    public Sent_Message getById(int id)  {
        return sent_messageRepository.getOne(id);
    }

    @Override
    @Transactional
    public int getSent_MessageId(Sent_Message sent_message){
        return sent_message.getId();
    }

    @Override
    public List<Sent_Message> getAllBySenderId(int idSender){
        return sent_messageRepository.findAllBySenderId(idSender);
    }

    @Override
    public List<Sent_Message> getAllByReceiverId(int idReceiver) {
        return sent_messageRepository.findAllByReceiverId(idReceiver);
    }

    @Override
    @Transactional
    public void delete(int id){
        sent_messageRepository.deleteById(id);
    }


    @Override
    public void save(Sent_Message sent_message){
        sent_messageRepository.save(sent_message);
    }

    @Override
    public Sent_Message findByMessageId(Message message) {
        return sent_messageRepository.findByMessageId(message.getId());
    }
}
