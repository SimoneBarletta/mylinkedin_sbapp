package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.Matching_JobApplication;
import it.unisalento.sbapp.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Matching_JobApplicationRepository extends JpaRepository<Matching_JobApplication, Integer> {

    List<Matching_JobApplication> findAll();
    List<Matching_JobApplication> getByUserId(int userId);
    List<Matching_JobApplication> getByPostId(int postId);
    List<Matching_JobApplication> getByApplierId(int applierId);
    List<Matching_JobApplication> getByStatusAndPostId(Boolean status,int postId);
    List<Matching_JobApplication> getByStatusAndUserId(Boolean status,int userId);
    List<Matching_JobApplication> getByStatusAndApplierId(Boolean status,int applierId);
    List<Matching_JobApplication> getByApplierIdOrUserId(int applierId, int userId);

    @Modifying
    @Query(value = "SELECT * FROM matching_job_application u where ( u.applier_id = ? OR u.user_id =  ?) and u.status = ?", nativeQuery = true)
    List<Matching_JobApplication> getByApplierIdOrUserIdAndStatus(int applierId, int userId, Boolean status);

   @Modifying
    @Query(value = "UPDATE matching_job_application m set m.status=1 where m.id=?",
            nativeQuery = true)
    void updateStatusTrue(int matchingJobId);
   @Modifying
    @Query(value = "UPDATE matching_job_application m set m.status=0 where m.id=?",
            nativeQuery = true)
    void updateStatusFalse(int matchingJobId);

   void deleteAllByUserId(int userId);





}
