package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.ProfileImage;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileImageRepository extends JpaRepository<ProfileImage, Integer> {

    @Modifying
    @Query(
            value = "DELETE FROM PROFILE_IMAGE i WHERE i.user_id= ?",
            nativeQuery = true)
    void deleteByUserId(Integer idUser);
    ProfileImage getByUserId(Integer userId);
}
