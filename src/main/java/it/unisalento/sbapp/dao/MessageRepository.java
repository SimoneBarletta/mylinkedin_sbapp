package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MessageRepository extends JpaRepository<Message,Integer> {



}
