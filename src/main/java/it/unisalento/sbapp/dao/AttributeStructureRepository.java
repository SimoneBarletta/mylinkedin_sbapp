package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.Attribute;
import it.unisalento.sbapp.domain.Structure;
import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.domain.AttributeStructure;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttributeStructureRepository extends JpaRepository<AttributeStructure, Integer> {

    List<AttributeStructure> findAll();
    List<AttributeStructure> getAllByStructureId(int idStructure);
    List<AttributeStructure> getAllByAttributeId(int idAttribute);

}
