package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.Offeror;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferorRepository extends JpaRepository<Offeror, Integer> {
}
