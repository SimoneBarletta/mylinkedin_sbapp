package it.unisalento.sbapp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.unisalento.sbapp.domain.Post;
import it.unisalento.sbapp.domain.User;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

	
	public List<Post> findByUserId(int userId);
	
	public List<Post> findByUser(User user);

	@Modifying
	@Query(value = "UPDATE Post p set p.hide = 1 where p.user_id = ?",
			nativeQuery = true)
	public void hidePostBannedUser(Integer id);

	@Modifying
	@Query(value = "UPDATE Post p set p.hide = 0 where p.user_id = ?",
			nativeQuery = true)
	public void showPostUnBannedUser(Integer id);


	@Modifying
	@Query(value = "UPDATE Post p set p.hide = 1 where p.id = ?",
			nativeQuery = true)
	public void hidePost(Integer id);

	@Modifying
	@Query(value = "UPDATE Post p set p.hide = 0 where p.id = ?",
			nativeQuery = true)
	public void showPost(Integer id);


	@Modifying
	@Query(value = "SELECT post.* FROM post INNER JOIN users AS u ON post.user_id = u.id and u.type != ? WHERE post.user_id != ? and post.hide = 0",
			nativeQuery = true)
	public List<Post> findPost(String type, int userId);

	@Modifying
	@Query(value = "SELECT post.* FROM post INNER JOIN attribute_value AS u ON post.id = u.post_id and u.content = ? and post.hide = 0",
			nativeQuery = true)
	public List<Post> findPostBySkill(String skill);

	@Modifying
	@Query(value = "SELECT * FROM post WHERE pubblication_date LIKE ?% and post.hide = 0",
			nativeQuery = true)
	public List<Post> findPostByDate(String date);



}
