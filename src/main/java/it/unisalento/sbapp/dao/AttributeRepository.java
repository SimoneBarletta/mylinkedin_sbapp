package it.unisalento.sbapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.domain.Attribute;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Integer> {

	Attribute getAttributeByName(String name);
    List<Attribute> findAll();
    List<Attribute> getByType(String type);

    @Modifying
    @Query(value = "UPDATE Attribute u set u.type = ?, u.description = ? where u.id = ?",
            nativeQuery = true)
    void update(String type,String description,Integer id);

}
