package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.AttributeValue;
import it.unisalento.sbapp.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface AttributeValueRepository extends JpaRepository<AttributeValue, Integer> {
    AttributeValue getAttributeValueById(Integer id);
    List<AttributeValue> getAllByPostId(Integer postId);
    List<AttributeValue> getAllByAttributeId(Integer attributeId);

    @Modifying
    @Query(value = "SELECT * FROM attribute_value as a INNER JOIN attribute as m ON a.attribute_id = m.id WHERE m.name = ? and a.post_id = ?",
            nativeQuery = true)
    List<AttributeValue> findAttributeByName(String attribute_name, int postId);

}
