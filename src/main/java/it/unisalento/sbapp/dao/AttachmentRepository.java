package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.Attachment;
import it.unisalento.sbapp.domain.Post;
import it.unisalento.sbapp.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {

    public List<Attachment> findByUser(User user);
    public List<Attachment> findByUserId(int userId);
    public List<Attachment> findByPostId(int postId);

    public void deleteByUserId(int userId);

}
