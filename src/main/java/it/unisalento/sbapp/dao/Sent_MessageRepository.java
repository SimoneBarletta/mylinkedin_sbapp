package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.Sent_Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Sent_MessageRepository extends JpaRepository<Sent_Message, Integer> {

    List<Sent_Message> findAllBySenderId(int idSender);
    List<Sent_Message> findAllByReceiverId(int idReceiver);
    Sent_Message findByMessageId(int idMessage);
}
