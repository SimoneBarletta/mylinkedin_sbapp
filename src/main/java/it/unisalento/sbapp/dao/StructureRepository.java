package it.unisalento.sbapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.domain.Structure;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StructureRepository extends JpaRepository<Structure, Integer> {

    Structure getByTitle(String title);
    List<Structure> findAll();
    Structure getById(int id);
    
    
}
