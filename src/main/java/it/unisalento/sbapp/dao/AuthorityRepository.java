package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}