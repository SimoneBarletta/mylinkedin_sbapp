package it.unisalento.sbapp.dao;

import it.unisalento.sbapp.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    List<Comment> getByPostId(Integer postId);
    List<Comment> getByPostIdAndParentId(Integer postId, Integer parentId);
    List<Comment> getChildrenByParentId(Integer parentId);

    void deleteByPostId(Integer postId);
    void deleteByParentId(Integer parentId);
    void deleteByUserId(Integer userId);

}
