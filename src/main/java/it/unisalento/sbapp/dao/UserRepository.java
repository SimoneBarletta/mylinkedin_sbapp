package it.unisalento.sbapp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.unisalento.sbapp.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	@Query(
			value = "SELECT * FROM  USERS  u where u.pending = 1",
			nativeQuery = true)
	public List<User> findPendingUsers();

	@Modifying
	@Query(value = "UPDATE Users u set u.disable = 1 where u.id = ?",
			nativeQuery = true)
	public void banById(Integer id);

	@Modifying
	@Query(value = "UPDATE Users u set u.disable = 0 where u.id = ?",
			nativeQuery = true)
	public void unbanById(Integer id);

	@Modifying
	@Query(value = "UPDATE Users u set u.pending = 0, u.enabled = 1 where u.id = ?",
			nativeQuery = true)
	public void acceptUser(Integer id);


	
	public List<User> findByName(String name);
	public User findByEmail(String email);

	public List<User> findBySurname(String surname);
	public List<User> findByNameAndSurname(String name, String surname);

	public List<User> findByType(String type);

	
}
