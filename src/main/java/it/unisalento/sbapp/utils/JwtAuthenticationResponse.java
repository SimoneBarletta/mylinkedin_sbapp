package it.unisalento.sbapp.utils;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;

public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String username;
    Collection<? extends GrantedAuthority> authorities;

    private final long expireIn;

    public JwtAuthenticationResponse(String username, Collection<? extends GrantedAuthority> authorities, long expireIn) {
        this.username = username;
        this.authorities = authorities;
        this.expireIn = expireIn;
    }

    public String getUsername() {
        return this.username;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public long getExpireIn() {
        return expireIn;
    }
}