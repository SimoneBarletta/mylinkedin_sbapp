package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.Exceptions.CommentNotFoundException;
import it.unisalento.sbapp.dao.CommentRepository;
import it.unisalento.sbapp.domain.Comment;
import it.unisalento.sbapp.dto.CommentDTO;

import java.util.List;

public interface ICommentService {

    void save(Comment comment);
    void delete(Integer id) throws CommentNotFoundException;
    void deleteAllByParent(Integer parentId) throws CommentNotFoundException;
    void deleteAllByPost(Integer postId) throws CommentNotFoundException;
    Comment getCommentById(Integer id) throws CommentNotFoundException;
    List<Comment> getByPostId(Integer postId) throws CommentNotFoundException;
    List<Comment> getByPostIdAndParentId(Integer postId, Integer parentId) throws CommentNotFoundException;
    List<Comment> getChildrenByParentId(Integer parentId) throws CommentNotFoundException;
    int getCommentId(CommentDTO commentDTO);

}
