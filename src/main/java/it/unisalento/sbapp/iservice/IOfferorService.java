package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.domain.Offeror;

import java.util.List;

public interface IOfferorService {
    List<Offeror> getAll();
}
