package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.Exceptions.AttributeStructureNotFoundException;
import it.unisalento.sbapp.domain.AttributeStructure;

import java.util.List;

public interface IAttributeStructureService {


    AttributeStructure getById(int id) throws AttributeStructureNotFoundException;
    List<AttributeStructure> getAll() throws AttributeStructureNotFoundException;
    List<AttributeStructure> getAllByStructureId(int idStructure) throws AttributeStructureNotFoundException;
    List<AttributeStructure> getAllByAttributeId(int idAttribute) throws AttributeStructureNotFoundException;
    void save(AttributeStructure attributeStructure);
    int getAttributeStructureId(AttributeStructure attributeStructure);
    void delete(int id);
}
