package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.Exceptions.*;
import it.unisalento.sbapp.domain.Attachment;


import java.util.List;

public interface IAttachmentService {

    Attachment getById(int id);

    int getAttachmentId(Attachment attachment);

    List<Attachment> getAttachmentByUserId(int userId) throws UserNotFoundExeception;
    List<Attachment> getAttachmentByPostId(int postId) throws PostNotFoundException;
    Attachment save(Attachment attachment) throws SavingUserException;
    void delete(int id) throws AttachmentNotFoundException;

    List<Attachment> getAll();
}
