package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.Exceptions.AttributeValueNotFoundException;
import it.unisalento.sbapp.domain.Attribute;
import it.unisalento.sbapp.domain.AttributeValue;

import java.util.List;

public interface IAttributeValueService {

    AttributeValue getById(int id) throws AttributeValueNotFoundException;
    List<AttributeValue> getAll() throws AttributeValueNotFoundException;
    List<AttributeValue> getAllByPostId(int idPost) throws AttributeValueNotFoundException;
    List<AttributeValue> getAllByAttributeId(int idAttribute) throws AttributeValueNotFoundException;
    void save(AttributeValue attributeValue);
    int getAttributeValueId(AttributeValue attributeValue);
    void delete(int id);
    List<AttributeValue> findAttributeByName(String attribute_name, int postId);

}
