package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.Exceptions.Matching_JobApplicationNotFoundException;
import it.unisalento.sbapp.domain.Matching_JobApplication;

import java.util.List;

public interface IMatching_JobApplicationService {
    
    void save(Matching_JobApplication matching_jobApplication);
    void delete(int id) throws  Matching_JobApplicationNotFoundException;
    Matching_JobApplication getById(int id) throws Matching_JobApplicationNotFoundException;
    int getMatching_JobApplicationId(Matching_JobApplication matching_jobApplication) throws Matching_JobApplicationNotFoundException;
    List<Matching_JobApplication> getAll() throws Matching_JobApplicationNotFoundException;
    List<Matching_JobApplication> getByUserId(int userId) throws Matching_JobApplicationNotFoundException;
    List<Matching_JobApplication> getByPostId(int postId) throws Matching_JobApplicationNotFoundException;
    List<Matching_JobApplication> getByStatusAndPostId(Boolean status,int postId) throws Matching_JobApplicationNotFoundException;
    List<Matching_JobApplication> getByStatusAndUserId(Boolean status,int userId) throws Matching_JobApplicationNotFoundException;
    List<Matching_JobApplication> getByApplierId(int applierId) throws Matching_JobApplicationNotFoundException;
    List<Matching_JobApplication> getByStatusAndApplierId(Boolean status,  int applierId) throws Matching_JobApplicationNotFoundException;
    List<Matching_JobApplication> getByApplierIdOrUserId(int applierId, int userId) throws Matching_JobApplicationNotFoundException;
    List<Matching_JobApplication> getByApplierIdOrUserIdAndStatus(int applierId, int userId, Boolean status) throws Matching_JobApplicationNotFoundException;

    void updateStatusTrue(int matchingJobId);
    void updateStatusFalse(int matchingJobId);
    void deleteAllByUserId(int userId);
}
