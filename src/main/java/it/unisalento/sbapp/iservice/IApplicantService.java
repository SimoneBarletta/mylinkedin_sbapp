package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.domain.Applicant;
import it.unisalento.sbapp.domain.Offeror;

import java.util.List;

public interface IApplicantService {
    List<Applicant> getAll();
}
