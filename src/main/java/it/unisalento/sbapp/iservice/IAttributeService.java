package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.Exceptions.AttributeNotFoundException;
import it.unisalento.sbapp.domain.Attribute;
import org.w3c.dom.Attr;

import java.util.List;

public interface IAttributeService {

    List<Attribute> getAll() throws AttributeNotFoundException;
    void save(Attribute attribute);
    Attribute getAttributeById(int id) throws AttributeNotFoundException;
    Attribute getAttributeByName(String name) throws AttributeNotFoundException;
    List<Attribute> getByType(String type) throws AttributeNotFoundException;
    void delete(int id) throws AttributeNotFoundException;
    int getAttributeId(Attribute attribute);
    void update(Attribute attribute);
}
