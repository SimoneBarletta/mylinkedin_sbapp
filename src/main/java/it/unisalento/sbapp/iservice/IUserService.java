package it.unisalento.sbapp.iservice;

import java.util.List;

import it.unisalento.sbapp.Exceptions.SavingUserException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.User;

public interface IUserService {

	User save(User user) throws SavingUserException;
	
	User getById(Integer id) throws UserNotFoundExeception;

	int getUserId(User user);

	List<User> getAll();
	
	void delete(int id) throws UserNotFoundExeception;

	User getByEmail(String email) throws UserNotFoundExeception;

	List<User> getByType(String type);

	void banUser(int id) throws UserNotFoundExeception;

	void unbanUser(int id) throws UserNotFoundExeception;

	List<User> getPendingUsers();

	void acceptUser(Integer id) throws  UserNotFoundExeception;
}
