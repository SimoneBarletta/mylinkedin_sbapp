package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.Exceptions.ProfileImageNotFoundException;
import it.unisalento.sbapp.Exceptions.StructureNotFoundException;
import it.unisalento.sbapp.domain.ProfileImage;
import it.unisalento.sbapp.domain.Structure;

import java.util.List;

public interface IStructureService {

    Structure getById(int id) throws StructureNotFoundException;
    void save(Structure structure);
    void delete(int id);

    int getStructureId(Structure structure) ;

    List<Structure> getAllStructures() throws StructureNotFoundException;

    Structure getByTitle(String title) throws StructureNotFoundException;
}
