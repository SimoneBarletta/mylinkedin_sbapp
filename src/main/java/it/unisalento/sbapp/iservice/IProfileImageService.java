package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.Exceptions.ProfileImageNotFoundException;
import it.unisalento.sbapp.Exceptions.SavingProfileImgException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.ProfileImage;

public interface IProfileImageService {

    ProfileImage getById(int id) throws ProfileImageNotFoundException;
    int getProfileImageId(ProfileImage profileImage);
    ProfileImage save(ProfileImage profileImage) throws SavingProfileImgException;
    ProfileImage getByUserId(Integer userId) throws UserNotFoundExeception;
    void deleteByUserId(Integer idUser) throws ProfileImageNotFoundException, UserNotFoundExeception;
}
