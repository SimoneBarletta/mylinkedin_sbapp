package it.unisalento.sbapp.iservice;
import it.unisalento.sbapp.Exceptions.MessageNotFoundException;
import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.domain.Sent_Message;

import java.util.List;

public interface IMessageService {

    int getMessageId(Message message);
    Message save(Message message);
    void delete(int id) throws MessageNotFoundException;
    Message getMessageById(int id) throws MessageNotFoundException;
    List<Message> filterMessagesByType(List<Sent_Message> sent_messageList, String type);

}
