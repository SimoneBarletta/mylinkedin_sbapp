package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.Exceptions.SavingSent_MessageException;
import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.domain.Sent_Message;

import java.util.List;

public interface ISent_MessageService {

    void delete(int id);
    Sent_Message getById(int id);
    int getSent_MessageId(Sent_Message sent_message);
    List<Sent_Message> getAllBySenderId(int idSender);
    List<Sent_Message> getAllByReceiverId(int idReceiver);
    void save(Sent_Message sent_message) throws SavingSent_MessageException;
    Sent_Message findByMessageId(Message message);

}
