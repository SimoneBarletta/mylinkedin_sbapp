package it.unisalento.sbapp.iservice;

import java.util.List;

import it.unisalento.sbapp.Exceptions.PostNotFoundException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.Post;

public interface IPostService {
	
	Post save(Post post);

	int getPostId(Post post);
	
	Post getById(int id) throws PostNotFoundException;
	
	List<Post> getAll();
	
	void deleteById(int id) throws PostNotFoundException;
	
	List<Post> getPostByUserId(int userId) throws UserNotFoundExeception;

	void hidePost(Integer id);

	void showPost(Integer id);

	List<Post> findByUserId(int userId) throws UserNotFoundExeception;

	List<Post> findPost(String type, int userId);

	List<Post> findPostBySkill(String skill);

	List<Post> findPostByDate(String date);


}
