package it.unisalento.sbapp.iservice;

import it.unisalento.sbapp.domain.Administrator;

import java.util.List;

public interface IAdministratorService {

    List<Administrator> getAll();
}
