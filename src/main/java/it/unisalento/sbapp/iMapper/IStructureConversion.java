package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Structure;
import it.unisalento.sbapp.dto.StructureDTO;

import java.util.List;

public interface IStructureConversion {

    List<StructureDTO> convertStructureListToStructureDTOList(List<Structure> structureList);
}
