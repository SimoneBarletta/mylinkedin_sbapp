package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Attribute;
import it.unisalento.sbapp.dto.AttributeDTO;
import it.unisalento.sbapp.iservice.IAttributeStructureService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses={IAttributeStructureService.class})
public interface IAttributeMapper {
    @Mappings({
            @Mapping(target="attributeStructureList", source = "idAttributeStructures")
    })
    Attribute convertAttributeDTOtoAttribute(AttributeDTO attributeDTO);

    @Mappings({
            @Mapping(source="attributeStructureList", target = "idAttributeStructures")
    })
    AttributeDTO convertAttributeToAttributeDTO(Attribute attribute);
}
