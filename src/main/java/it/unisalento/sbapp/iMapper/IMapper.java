package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.dto.UserDTO;

import java.util.List;

public interface IMapper {

    User convertToUser(UserDTO body) ;
    List<UserDTO> convertToUserDTOList(List<User> userList);
    UserDTO convertUserToUserDTO(User user);
}
