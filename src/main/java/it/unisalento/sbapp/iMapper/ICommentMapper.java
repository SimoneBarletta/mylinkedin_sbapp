package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Comment;
import it.unisalento.sbapp.dto.CommentDTO;
import it.unisalento.sbapp.iservice.ICommentService;
import it.unisalento.sbapp.iservice.IPostService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {IPostService.class, ICommentService.class})
public interface ICommentMapper {


    @Mappings({
            @Mapping(source = "postId", target = "post"),
           // @Mapping(source = "parentId", target = "parent"),
            @Mapping(source = "childrenId", target = "children")
    })
    Comment convertCommentDTOToComment(CommentDTO commentDTO);

    @Mappings({
            @Mapping(target = "postId", source = "post"),
            //@Mapping(target = "parentId", source = "parent"),
            @Mapping(target = "childrenId", source = "children")

    })
    CommentDTO convertCommentToCommentDTO(Comment comment);
}
