package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Post;
import it.unisalento.sbapp.dto.PostDTO;

import java.util.List;

public interface IPostConversion {

    List<PostDTO> convertPostListToDTOList(List<Post> postList);
}
