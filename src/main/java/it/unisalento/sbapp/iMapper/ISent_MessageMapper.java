package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Sent_Message;
import it.unisalento.sbapp.dto.Sent_MessageDTO;
import it.unisalento.sbapp.iservice.IMessageService;
import it.unisalento.sbapp.iservice.IUserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {IMessageService.class, IUserService.class})
public interface ISent_MessageMapper {

    @Mappings({
            @Mapping(source = "idMessage", target = "message"),
            @Mapping(source = "idSender", target = "sender"),
            @Mapping(source = "idReceiver", target = "receiver")
    }
    )
    Sent_Message convertSentMessageDTOToSentMessage(Sent_MessageDTO sent_messageDTO);

    @Mappings({
            @Mapping(target = "idMessage", source = "message"),
            @Mapping(target = "idSender", source = "sender"),
            @Mapping(target = "idReceiver", source = "receiver")
    }
    )
    Sent_MessageDTO convertSentMessageToSentMessageDTO(Sent_Message sent_message);

}
