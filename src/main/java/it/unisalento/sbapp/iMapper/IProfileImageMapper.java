package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.Exceptions.SavingProfileImgException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.ProfileImage;
import it.unisalento.sbapp.dto.ProfileImageDTO;
import org.json.simple.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface IProfileImageMapper {

    ProfileImage convertProfileImageDTOToProfileImage(ProfileImageDTO profileImageDTO,String filePath) throws UserNotFoundExeception;
    ProfileImageDTO convertProfileImageToDTO(ProfileImage profileImage) throws IOException;
    JSONObject convertProfileImgDTOtoJson(ProfileImageDTO profileImageDTO) throws IOException;
}
