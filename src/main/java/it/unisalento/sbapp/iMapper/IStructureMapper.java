package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Structure;
import it.unisalento.sbapp.dto.StructureDTO;
import it.unisalento.sbapp.iservice.IAttributeStructureService;
import it.unisalento.sbapp.iservice.IPostService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses={IAttributeStructureService.class, IPostService.class})
public interface IStructureMapper {

    @Mappings({
            @Mapping(source = "idsPost", target = "postList"),
            @Mapping(source = "idsAttributeStructure", target = "attributeStructureList")
    })
    Structure convertStructureDTOToStructure(StructureDTO structureDTO);

    @Mappings({
            @Mapping(target = "idsPost", source = "postList"),
            @Mapping(target = "idsAttributeStructure", source = "attributeStructureList")
    })
    StructureDTO convertStructureToStructureDTO(Structure structure);
}
