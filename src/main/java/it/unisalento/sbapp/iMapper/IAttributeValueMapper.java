package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.AttributeValue;
import it.unisalento.sbapp.dto.AttributeValueDTO;
import it.unisalento.sbapp.iservice.IAttributeService;
import it.unisalento.sbapp.iservice.IPostService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses={IPostService.class, IAttributeService.class})
public interface IAttributeValueMapper {

    @Mappings({
            @Mapping(target="post", source = "postId"),
            @Mapping(target="attribute", source = "attributeId")
    })
    AttributeValue convertAttributeValueDTOtoAttributeValue(AttributeValueDTO attributeValueDTO);

    @Mappings({
            @Mapping(target="postId", source = "post"),
            @Mapping(target="attributeId", source = "attribute")
    })
    AttributeValueDTO convertAttributeValueToAttributeValueDTO(AttributeValue attributeValue);
}
