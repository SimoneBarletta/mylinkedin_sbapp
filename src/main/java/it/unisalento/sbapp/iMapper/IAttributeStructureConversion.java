package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.AttributeStructure;
import it.unisalento.sbapp.dto.AttributeStructureDTO;

import java.util.List;

public interface IAttributeStructureConversion {

    List<AttributeStructureDTO> convertAttrStructListToAttrStrucDTOList(List<AttributeStructure> attributeStructureList);
}
