package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.AttributeValue;
import it.unisalento.sbapp.dto.AttributeValueDTO;

import java.util.List;

public interface IAttributeValueConversion {

    List<AttributeValueDTO> convertAttrValueListToAttrValueDTOList(List<AttributeValue> attributeValueList);

}
