package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.domain.Sent_Message;
import it.unisalento.sbapp.dto.Sent_MessageDTO;

import java.util.List;

public interface ISent_MessageConversion {

    List<Sent_MessageDTO> convertSentMessageListToDTOList(List<Sent_Message> sent_messageList);
}
