package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Matching_JobApplication;
import it.unisalento.sbapp.dto.Matching_JobApplicationDTO;

import java.util.List;

public interface IMatching_JobApplicationConversion {
    
    List<Matching_JobApplicationDTO> convertMatchListToMatchDTOList(List<Matching_JobApplication> matching_jobApplicationList);
}
