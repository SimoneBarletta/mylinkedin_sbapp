package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Attribute;
import it.unisalento.sbapp.domain.AttributeStructure;
import it.unisalento.sbapp.dto.AttributeStructureDTO;
import it.unisalento.sbapp.iservice.IAttributeService;
import it.unisalento.sbapp.iservice.IStructureService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {IAttributeService.class, IStructureService.class})
public interface IAttributeStructureMapper {

    @Mappings({
            @Mapping(source = "idAttribute", target ="attribute"),
            @Mapping(source = "idStructure", target ="structure")
    })
    AttributeStructure convertAttrStrucDTOtoAttrStruct(AttributeStructureDTO attributeStructureDTO);

    @Mappings({
            @Mapping(target = "idAttribute", source ="attribute"),
            @Mapping(target = "idStructure", source ="structure")
    })
    AttributeStructureDTO convertAttrStructoAttrStructDTO(AttributeStructure attributeStructure);
}
