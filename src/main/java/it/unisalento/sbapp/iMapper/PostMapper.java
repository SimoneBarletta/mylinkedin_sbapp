package it.unisalento.sbapp.iMapper;


import it.unisalento.sbapp.domain.Post;
import it.unisalento.sbapp.dto.PostDTO;
import it.unisalento.sbapp.iservice.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {IMatching_JobApplicationService.class,
        IStructureService.class,IUserService.class, IAttributeValueService.class})
public interface PostMapper {



    @Mappings({
            @Mapping(target="structure", source = "idStructure"),
            @Mapping(target="user", source = "idUser"),
            //@Mapping(target="matching_jobApplicationList", source = "idsMatching_JobApplication"),
           // @Mapping(target="attributeValueList", source = "idsAttributeValue")
    })
    Post postDTOtoPost(PostDTO postDTO);


    @Mappings({
            @Mapping(target="idStructure", source = "structure"),
            @Mapping(target="idUser", source = "user"),
            //@Mapping(target="idsMatching_JobApplication", source = "matching_jobApplicationList"),
           // @Mapping(target="idsAttributeValue", source = "attributeValueList")
    })
    PostDTO postToPostDTO(Post post);



}
