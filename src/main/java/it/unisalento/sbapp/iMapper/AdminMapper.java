package it.unisalento.sbapp.iMapper;


import it.unisalento.sbapp.domain.Administrator;
import it.unisalento.sbapp.domain.Offeror;
import it.unisalento.sbapp.dto.AdministratorDTO;
import it.unisalento.sbapp.dto.OfferorDTO;
import it.unisalento.sbapp.iservice.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;

@Mapper(componentModel = "spring", uses = {IProfileImageService.class, IPostService.class, IAttachmentService.class,
        IMatching_JobApplicationService.class,
        ISent_MessageService.class})
public interface AdminMapper {


    @Mappings({
            @Mapping(target="phone_number", source = "administratorDto.phone"),
            @Mapping(target="profileImage", source = "idProfileImage"),
            @Mapping(target="postList", source = "idPosts"),
            @Mapping(target="attachmentList", source = "idAttachments"),
            @Mapping(target="matching_jobApplicationList", source = "idMatchedJobs"),
            @Mapping(target="sentMessageList", source = "idSentMessages"),
            @Mapping(target="receivedMessageList", source = "idReceivedMessages")

    })
    Administrator adminDTOtoAdministrator(AdministratorDTO administratorDto);

    @Mappings({
            @Mapping(target="phone", source = "admin.phone_number"),
            //@Mapping(source="profileImage", target = "idProfileImage"),
            @Mapping(source="postList", target = "idPosts"),
            @Mapping(source="attachmentList", target = "idAttachments"),
            @Mapping(source="matching_jobApplicationList", target = "idMatchedJobs"),
            @Mapping(source="sentMessageList", target = "idSentMessages"),
            @Mapping(source="receivedMessageList", target = "idReceivedMessages")})
    AdministratorDTO administratorToAdminDTO(Administrator admin);

}

