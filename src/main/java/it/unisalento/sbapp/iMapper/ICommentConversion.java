package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Comment;
import it.unisalento.sbapp.dto.CommentDTO;

import java.util.List;

public interface ICommentConversion {

    List<CommentDTO> convertCommentListToCommentDTOList(List<Comment> commentList);
}
