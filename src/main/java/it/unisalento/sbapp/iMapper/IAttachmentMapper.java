package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.Exceptions.AttributeNotFoundException;
import it.unisalento.sbapp.Exceptions.PostNotFoundException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.Attachment;
import it.unisalento.sbapp.dto.AttachmentDTO;
import org.json.simple.JSONArray;

import java.io.IOException;
import java.util.List;


public interface IAttachmentMapper {

    Attachment convertAttachmentDTOtoAttachment(AttachmentDTO attachmentDTO, String path) throws UserNotFoundExeception, AttributeNotFoundException, PostNotFoundException;
    AttachmentDTO convertAttachmentToDTO(Attachment attachment) throws IOException;
    List<AttachmentDTO> convertListAttachmentToDTOs(List<Attachment> attachmentList) throws IOException;
    JSONArray convertAttachmentDTOListToJson(List<AttachmentDTO> attachmentDTOList) throws IOException ;
}
