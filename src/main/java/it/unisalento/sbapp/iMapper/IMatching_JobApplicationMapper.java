package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Matching_JobApplication;
import it.unisalento.sbapp.dto.Matching_JobApplicationDTO;
import it.unisalento.sbapp.iservice.IPostService;
import it.unisalento.sbapp.iservice.IUserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {IUserService.class, IPostService.class})
public interface IMatching_JobApplicationMapper {

    @Mappings({
           @Mapping(source = "idUser", target = "user"),
            @Mapping(source = "idApplier", target = "applier"),
           @Mapping(source = "idPost", target = "post")
    })
    Matching_JobApplication convertMatchDTOToMatchDomain(Matching_JobApplicationDTO matching_jobApplicationDTO);

    @Mappings({
            @Mapping(target = "idUser", source = "user"),
            @Mapping(target = "idApplier", source = "applier"),
            @Mapping(target = "idPost", source = "post")
    })
    Matching_JobApplicationDTO convertMatchDomainToMatchDTO(Matching_JobApplication matching_jobApplication);

}
