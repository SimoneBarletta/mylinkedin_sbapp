package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.dto.MessageDTO;

import java.util.List;

public interface IMessageConversion {

    List<MessageDTO> convertMessageListToMessageDTOList(List<Message> messageList);
}
