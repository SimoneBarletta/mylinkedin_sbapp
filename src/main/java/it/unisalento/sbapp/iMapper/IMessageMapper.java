package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.dto.MessageDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IMessageMapper {

    Message convertMessageDTOToMessage(MessageDTO messageDTO);
    MessageDTO convertMessageToMessageDTO(Message message);
}
