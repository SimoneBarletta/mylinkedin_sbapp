package it.unisalento.sbapp.iMapper;

import it.unisalento.sbapp.domain.Attribute;
import it.unisalento.sbapp.dto.AttributeDTO;

import java.util.List;

public interface IAttributeConversion {


    List<AttributeDTO> convertAttributeListToAttributeDTOList(List<Attribute> attributeList);
}
