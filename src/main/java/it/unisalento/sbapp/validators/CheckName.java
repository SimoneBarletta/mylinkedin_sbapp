package it.unisalento.sbapp.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CheckNameValidator.class)
public @interface CheckName {
	
	String [] nameToCheck ();
	
	String message() default "Nome non valido";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
