package it.unisalento.sbapp.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;

public class MatchFieldsValidator implements ConstraintValidator<MatchFieldsConstraint, Object> {

	String field;
	String fieldMatch;
	
	@Override
	public void initialize(MatchFieldsConstraint constraintAnnotation) {
		// TODO Auto-generated method stub
		this.field = constraintAnnotation.field();
		this.fieldMatch = constraintAnnotation.fieldMatch();
	}
	
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		
		Object fieldValue = new BeanWrapperImpl(value).getPropertyValue(field);
		Object fieldMatchValue = new BeanWrapperImpl(value).getPropertyValue(fieldMatch);
		
		if (fieldValue.equals(fieldMatchValue)) {
			return true; 
		}
		
		return false;
	}

}
