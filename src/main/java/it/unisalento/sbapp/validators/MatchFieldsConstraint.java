package it.unisalento.sbapp.validators;

import java.lang.annotation.*;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MatchFieldsValidator.class)
@Target(ElementType.TYPE)
public @interface MatchFieldsConstraint {
	
	String field ();
	String fieldMatch ();
	
	String message() default "I campi devono essere uguali";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};


}
