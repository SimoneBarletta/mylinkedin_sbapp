package it.unisalento.sbapp.pushNotification;

import com.google.firebase.messaging.FirebaseMessagingException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.dto.SaveTokenDTO;

import javax.transaction.Transactional;

public interface FirebaseService {


    @Transactional
    String getToken(Integer userId) throws UserNotFoundExeception;

    @Transactional
    void saveToken(SaveTokenDTO saveTokenDTO) throws UserNotFoundExeception;

    void sendNotification(String title, String detail, String tokenUser) throws FirebaseMessagingException;
}
