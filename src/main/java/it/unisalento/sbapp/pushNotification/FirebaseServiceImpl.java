package it.unisalento.sbapp.pushNotification;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.dao.UserRepository;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.dto.SaveTokenDTO;
import it.unisalento.sbapp.pushNotification.FirebaseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class FirebaseServiceImpl implements FirebaseService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public String getToken(Integer userId) throws UserNotFoundExeception {

        User user = userRepository.findById(userId)
                .orElseThrow(UserNotFoundExeception::new);

        return user.getFirebaseToken();
    }

    @Override
    @Transactional
    public void saveToken(SaveTokenDTO saveTokenDTO) throws UserNotFoundExeception {

        User user = userRepository.findById(saveTokenDTO.getIdUser()).orElseThrow(UserNotFoundExeception::new);

        user.setFirebaseToken(saveTokenDTO.getToken());

        userRepository.save(user);
    }

    @Override
    public void sendNotification(String title, String detail, String firebaseToken) throws FirebaseMessagingException {

        Notification notification = Notification.builder()
                .setTitle(title)
                .setBody(detail)
                .build();

        Message message = Message.builder()
                .setNotification(notification)
                .setToken(firebaseToken)
                .build();

        FirebaseMessaging.getInstance().send(message);
    }
}
