package it.unisalento.sbapp.pushNotification;

import it.unisalento.sbapp.dto.SaveTokenDTO;
import it.unisalento.sbapp.pushNotification.FirebaseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class FirebaseController {

    private final FirebaseService firebaseService;

    @PostMapping("/public/firebase/token")
    public void saveToken(@RequestBody SaveTokenDTO saveTokenDTO) {
        try {
            firebaseService.saveToken(saveTokenDTO);
        } catch (Exception e) {
            log.error("Failed to save firebase token of user {}");
        }
    }



}
