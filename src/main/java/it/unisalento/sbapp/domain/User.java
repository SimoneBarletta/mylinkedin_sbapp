package it.unisalento.sbapp.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Table(name = "USERS")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;

	@NotNull
	String name;
	@NotNull
	String surname;

	@NotNull
	@Column(unique = true)
	String email;

	@NotNull
	int age;

	@NotNull
	@Column(name = "PASSWORD")
	String password;

	@NotNull
	String phone_number;

	@Column(name = "type", insertable=false, updatable=false)
	private String type;

	@Column(name = "ENABLED")
	@NotNull
	private Boolean enabled;

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public void setType(String type) {
		this.type = type;
	}

	@OneToOne(mappedBy = "user", targetEntity = ProfileImage.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	ProfileImage profileImage;
	
	@OneToMany(mappedBy = "user", targetEntity = Post.class, cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	List<Post> postList;

	@OneToMany(mappedBy = "user", targetEntity = Attachment.class, cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	List<Attachment> attachmentList;

	@OneToMany(mappedBy = "user", targetEntity = Matching_JobApplication.class, cascade =CascadeType.ALL , fetch = FetchType.LAZY)
	List<Matching_JobApplication> matching_jobApplicationList;

	@OneToMany(mappedBy = "sender", targetEntity = Sent_Message.class, cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	List<Sent_Message> sentMessageList;

	@OneToMany(mappedBy = "receiver", targetEntity = Sent_Message.class, cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	List<Sent_Message> receivedMessageList;

	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JoinTable(
			name = "USERS_AUTHORITIES",
			joinColumns = {@JoinColumn(name = "USER_id", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "AUTHORITY_ID", referencedColumnName = "ID")})
	private List<Authority> authorities;

	private String firebaseToken;

	public String getFirebaseToken() {
		return firebaseToken;
	}

	public void setFirebaseToken(String firebaseToken) {
		this.firebaseToken = firebaseToken;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public List<Attachment> getAttachmentList() {
		return attachmentList;
	}
	public void setAttachmentList(List<Attachment> attachmentList) {
		this.attachmentList = attachmentList;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public ProfileImage getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(ProfileImage profileImage) {
		this.profileImage = profileImage;
	}
	public List<Post> getPostList() {
		return postList;
	}
	public void setPostList(List<Post> postList) {
		this.postList = postList;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public List<Matching_JobApplication> getMatching_jobApplicationList() {
		return matching_jobApplicationList;
	}

	public void setMatching_jobApplicationList(List<Matching_JobApplication> matching_jobApplicationList) {
		this.matching_jobApplicationList = matching_jobApplicationList;
	}

	public List<Sent_Message> getSentMessageList() {
		return sentMessageList;
	}

	public void setSentMessageList(List<Sent_Message> sentMessageList) {
		this.sentMessageList = sentMessageList;
	}

	public List<Sent_Message> getReceivedMessageList() {
		return receivedMessageList;
	}

	public void setReceivedMessageList(List<Sent_Message> receivedMessageList) {
		this.receivedMessageList = receivedMessageList;
	}

	public List<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}

	@Transient
	public String getType() {
		return this.getClass().getAnnotation(DiscriminatorValue.class).value();
	}
}
