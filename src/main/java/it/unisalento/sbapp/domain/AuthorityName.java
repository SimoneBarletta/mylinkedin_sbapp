package it.unisalento.sbapp.domain;

public enum AuthorityName {
    ROLE_USER, ROLE_OFFEROR, ROLE_ADMIN, ROLE_APPLICANT
}