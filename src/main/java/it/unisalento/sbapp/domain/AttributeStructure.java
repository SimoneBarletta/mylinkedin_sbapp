package it.unisalento.sbapp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AttributeStructure {

	
	public AttributeStructure() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public AttributeStructure(int id, Attribute attribute, Structure structure) {
		super();
		this.id = id;
		this.attribute = attribute;
		this.structure = structure;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	int id;
	
	
	@ManyToOne(optional = false)
	Attribute attribute;
	
	@ManyToOne(optional = false) 
	Structure structure;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public Structure getStructure() {
		return structure;
	}

	public void setStructure(Structure structure) {
		this.structure = structure;
	}
	
	
	
}
