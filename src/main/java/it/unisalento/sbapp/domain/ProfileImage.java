package it.unisalento.sbapp.domain;

import javax.persistence.*;

@Entity
public class ProfileImage {
	
	public ProfileImage() {
		// TODO Auto-generated constructor stub
	}
	
	public ProfileImage(int id, String description, User user, String file_path) {
		super();
		this.id = id;
		this.description = description;
		this.user = user;
		this.file_path = file_path;
	}

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	int id;


	String description;
	
	@OneToOne(optional=false)
	User user;

	String file_path;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
}
