package it.unisalento.sbapp.domain;

import java.util.List;

import javax.persistence.*;

@Entity
public class Attribute {

	public Attribute() {
		// TODO Auto-generated constructor stub
	}
	
	public Attribute(int id, String name, String type, List<AttributeStructure> attributeStructureList, String description) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.attributeStructureList = attributeStructureList;
		this.description = description;
	}
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	int id;

	@Column(unique = true)
	String name;
	String type;

	@Column(length = 800)
	String description;
	
	
	@OneToMany(mappedBy = "attribute", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<AttributeStructure> attributeStructureList;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<AttributeStructure> getAttributeStructureList() {
		return attributeStructureList;
	}
	public void setAttributeStructureList(List<AttributeStructure> attributeStructureList) {
		this.attributeStructureList = attributeStructureList;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
