package it.unisalento.sbapp.domain;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
public class Matching_JobApplication {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	int id;
	boolean status;

	@ManyToOne(optional = false)
	User user;

	@ManyToOne(optional = false)
	User applier;

	@ManyToOne(optional = false)
	Post post;
	Timestamp timestamp;

	public Matching_JobApplication(int id, boolean status, User user, Post post, Timestamp timestamp, User applier) {
		this.id = id;
		this.status = status;
		this.user = user;
		this.post = post;
		this.timestamp = timestamp;
		this.applier = applier;
	}

	public Matching_JobApplication(){

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}


	public User getApplier() {
		return applier;
	}

	public void setApplier(User applier) {
		this.applier = applier;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
}
