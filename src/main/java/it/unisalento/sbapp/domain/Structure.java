package it.unisalento.sbapp.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Structure {

	
	
	public Structure() {
		// TODO Auto-generated constructor stub
	}

	
	
	public Structure(int id, String title, String description, List<Post> postList,
			List<AttributeStructure> attributeStructureList) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.postList = postList;
		this.attributeStructureList = attributeStructureList;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;

	@Column(unique = true)
	String title;
	
	@Column(length = 800)
	String description;
	
	@OneToMany(mappedBy = "structure", targetEntity = Post.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	List<Post> postList;
	
	
	@OneToMany(mappedBy = "structure", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<AttributeStructure> attributeStructureList;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Post> getPostList() {
		return postList;
	}
	public void setPostList(List<Post> postList) {
		this.postList = postList;
	}
	public List<AttributeStructure> getAttributeStructureList() {
		return attributeStructureList;
	}
	public void setAttributeStructureList(List<AttributeStructure> attributeStructureList) {
		this.attributeStructureList = attributeStructureList;
	}
}
