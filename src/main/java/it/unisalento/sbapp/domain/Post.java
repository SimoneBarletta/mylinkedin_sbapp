package it.unisalento.sbapp.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class Post {
	

	public Post(int id, Date pubblicationDate, boolean hide, Structure structure, User user) {
		super();
		this.id = id;
		this.pubblicationDate = pubblicationDate;
		this.hide = hide;
		this.structure = structure;
		this.user = user;
		//this.matching_jobApplicationList = matching_jobApplicationList;
	}
	
	public Post() {
		// TODO Auto-generated constructor stub
	}
	


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	int id;
	Date pubblicationDate;
	boolean hide;

	float lat;
	float lan;


	@ManyToOne(optional = false, fetch = FetchType.EAGER)

	Structure structure;
	
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	User user;
/*

	@OneToMany(mappedBy = "user", targetEntity = Matching_JobApplication.class, cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	List<Matching_JobApplication> matching_jobApplicationList;


	@OneToMany(mappedBy ="post")
	List<AttributeValue> attributeValueList;

	public List<AttributeValue> getAttributeValueList() {
		return attributeValueList;
	}

	public void setAttributeValueList(List<AttributeValue> attributoValueList) {
		this.attributeValueList = attributoValueList;
	}
*/

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public Date getPubblicationDate() {
		return pubblicationDate;
	}
	public void setPubblicationDate(Date pubblicationDate) {
		this.pubblicationDate = pubblicationDate;
	}

	public boolean isHide() {
		return hide;
	}
	public void setHide(boolean hide) {
		this.hide = hide;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public Structure getStructure() {
		return structure;
	}
	public void setStructure(Structure structure) {
		this.structure = structure;
	}

	public float getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public float getLan() {
		return lan;
	}

	public void setLan(float lan) {
		this.lan = lan;
	}

	/*
	public List<Matching_JobApplication> getMatching_jobApplicationList() {
		return matching_jobApplicationList;
	}
	public void setMatching_jobApplicationList(List<Matching_JobApplication> matching_jobApplicationList) {
		this.matching_jobApplicationList = matching_jobApplicationList;
	}*/
}
