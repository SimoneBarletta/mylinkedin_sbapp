package it.unisalento.sbapp.domain;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
public class Message {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	Timestamp timestamp;
	String type;
	@Column(length = 2000)
	String content;

	public Message(int id, Timestamp timestamp, String type, String content) {
		this.id = id;
		this.timestamp = timestamp;
		this.type = type;
		this.content = content;
	}

	public Message(){

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
