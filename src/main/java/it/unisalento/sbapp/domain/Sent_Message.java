package it.unisalento.sbapp.domain;


import javax.persistence.*;

@Entity
public class Sent_Message {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;

	@ManyToOne(optional = false)
	Message message;

	@ManyToOne(optional = false)
	User sender;

	@ManyToOne(optional = false)
	User receiver;

	public Sent_Message(int id, Message message, User sender, User receiver) {
		this.id = id;
		this.message = message;
		this.sender = sender;
		this.receiver = receiver;
	}

	public Sent_Message(){

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}
}
