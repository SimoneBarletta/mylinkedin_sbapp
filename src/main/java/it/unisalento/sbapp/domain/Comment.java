package it.unisalento.sbapp.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Comment {

    public Comment(){

    }

    public Comment(int id, String data, Comment parent, List<Comment> children, Post post, User user) {
        super();
        this.id = id;
        this.data = data;
        this.parent = parent;
        this.children = children;
        this.post = post;
        this.user = user;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @Column(nullable = false)
    String data;

    @ManyToOne(fetch = FetchType.LAZY)
    Comment parent;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "parent", cascade = CascadeType.REMOVE)
    List<Comment> children;

    @ManyToOne
    Post post;

    @ManyToOne
    User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Comment getParent() {
        return parent;
    }

    public void setParent(Comment parent) {
        this.parent = parent;
    }

    public List<Comment> getChildren() {
        return children;
    }

    public void setChildren(List<Comment> children) {
        this.children = children;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
