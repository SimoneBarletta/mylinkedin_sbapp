package it.unisalento.sbapp;

import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.iservice.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication
public class SbAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbAppApplication.class, args);
	}




}
