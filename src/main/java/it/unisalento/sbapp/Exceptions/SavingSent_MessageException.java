package it.unisalento.sbapp.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Salvataggio del Sent_Message non riuscito")
public class SavingSent_MessageException extends Exception{
}
