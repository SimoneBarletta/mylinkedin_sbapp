package it.unisalento.sbapp.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Matching_JobApplication non trovato")
public class Matching_JobApplicationNotFoundException extends Exception{
}
