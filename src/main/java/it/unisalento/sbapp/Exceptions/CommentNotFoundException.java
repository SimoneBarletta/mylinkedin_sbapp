package it.unisalento.sbapp.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Commento non trovato")
public class CommentNotFoundException extends Exception{
}
