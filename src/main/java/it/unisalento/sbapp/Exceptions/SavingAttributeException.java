package it.unisalento.sbapp.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Errore nel salvataggio dell'attribute")
public class SavingAttributeException extends Exception{
}
