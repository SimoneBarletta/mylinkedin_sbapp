package it.unisalento.sbapp.dto;

import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

public class Matching_JobApplicationDTO {

    Integer id;
    boolean status;
    Integer idUser;
    Integer idApplier;
    Integer idPost;
    Timestamp timestamp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdPost() {
        return idPost;
    }

    public void setIdPost(Integer idPost) {
        this.idPost = idPost;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getIdApplier() {
        return idApplier;
    }

    public void setIdApplier(Integer idApplier) {
        this.idApplier = idApplier;
    }
}
