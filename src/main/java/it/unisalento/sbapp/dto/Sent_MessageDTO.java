package it.unisalento.sbapp.dto;

import javax.validation.constraints.NotBlank;

public class Sent_MessageDTO {

    Integer Id;
    Integer idMessage;
    Integer idSender;
    Integer IdReceiver;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(Integer idMessage) {
        this.idMessage = idMessage;
    }

    public Integer getIdSender() {
        return idSender;
    }

    public void setIdSender(Integer idSender) {
        this.idSender = idSender;
    }

    public Integer getIdReceiver() {
        return IdReceiver;
    }

    public void setIdReceiver(Integer idReceiver) {
        IdReceiver = idReceiver;
    }
}
