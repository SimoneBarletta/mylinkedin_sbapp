package it.unisalento.sbapp.dto;

public class SaveTokenDTO {
    private String token;
    private int idUser;

    public SaveTokenDTO(String token, int idUser) {
        this.token = token;
        this.idUser = idUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
}
