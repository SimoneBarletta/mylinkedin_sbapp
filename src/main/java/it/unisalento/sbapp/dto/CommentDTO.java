package it.unisalento.sbapp.dto;

import java.util.List;

public class CommentDTO {

    Integer id;
    String data;
    Integer parentId;
    List<Integer> childrenId;
    Integer postId;
    Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<Integer> getChildrenId() {
        return childrenId;
    }

    public void setChildrenId(List<Integer> childrenId) {
        this.childrenId = childrenId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }
}
