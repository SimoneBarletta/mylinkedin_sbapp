package it.unisalento.sbapp.dto;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class AttributeDTO{

    Integer id;
    @NotBlank
    String name;
    @NotBlank
    String type;
    List<Integer> idAttributeStructures;
    @NotBlank
    String description;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Integer> getIdAttributeStructures() {
        return idAttributeStructures;
    }

    public void setIdAttributeStructures(List<Integer> idAttributeStructures) {
        this.idAttributeStructures = idAttributeStructures;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
