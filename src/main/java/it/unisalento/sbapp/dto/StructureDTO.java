package it.unisalento.sbapp.dto;

import java.util.List;

public class StructureDTO {

    Integer id;
    String title;
    String description;
    List<Integer> idsPost;
    List<Integer> idsAttributeStructure;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getIdsPost() {
        return idsPost;
    }

    public void setIdsPost(List<Integer> idsPost) {
        this.idsPost = idsPost;
    }

    public List<Integer> getIdsAttributeStructure() {
        return idsAttributeStructure;
    }

    public void setIdsAttributeStructure(List<Integer> idsAttributeStructure) {
        this.idsAttributeStructure = idsAttributeStructure;
    }
}
