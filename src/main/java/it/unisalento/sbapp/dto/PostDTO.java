package it.unisalento.sbapp.dto;

import java.util.Date;

public class PostDTO {

	int id;
	Date pubblicationDate;
	boolean hide;
	int  idStructure;
	int idUser;
	float lan;
	float lat;

	/*
	List<Integer> idsMatching_JobApplication;
	List<Integer> idsAttributeValue;

	public List<Integer> getIdsAttributeValue() {
		return idsAttributeValue;
	}

	public void setIdsAttributeValue(List<Integer> idsAttributeValue) {
		this.idsAttributeValue = idsAttributeValue;
	}

	public List<Integer> getIdsMatching_JobApplication() {
		return idsMatching_JobApplication;
	}
	public void setIdsMatching_JobApplication(List<Integer> idsMatching_JobApplication) {
		this.idsMatching_JobApplication = idsMatching_JobApplication;
	}*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public Date getPubblicationDate() {
		return pubblicationDate;
	}

	public void setPubblicationDate(Date pubblicationDate) {
		this.pubblicationDate = pubblicationDate;
	}

	public boolean isHide() {
		return hide;
	}
	public void setHide(boolean hide) {
		this.hide = hide;
	}
	public int getIdStructure() {
		return idStructure;
	}
	public void setIdStructure(int idStructure) {
		this.idStructure = idStructure;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public float getLan() {
		return lan;
	}

	public void setLan(float lan) {
		this.lan = lan;
	}

	public float getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}
}
