package it.unisalento.sbapp.dto;



import javax.validation.constraints.NotBlank;

public class AttributeStructureDTO{

    Integer id;
    @NotBlank
    int idAttribute;
    @NotBlank
    int idStructure;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdAttribute() {
        return idAttribute;
    }

    public void setIdAttribute(int idAttribute) {
        this.idAttribute = idAttribute;
    }

    public int getIdStructure() {
        return idStructure;
    }

    public void setIdStructure(int idStructure) {
        this.idStructure = idStructure;
    }


}
