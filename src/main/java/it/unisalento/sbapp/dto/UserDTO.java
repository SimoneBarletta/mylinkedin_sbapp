package it.unisalento.sbapp.dto;

import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import it.unisalento.sbapp.validators.MatchFieldsConstraint;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
		@JsonSubTypes.Type(value = OfferorDTO.class, name = "offeror"),
		@JsonSubTypes.Type(value = ApplicantDTO.class, name = "applicant"),
		@JsonSubTypes.Type(value = AdministratorDTO.class, name = "admin")
})
@MatchFieldsConstraint(field = "password", fieldMatch = "passwordVerify")
public class UserDTO {

	Integer id;
	@NotBlank
	String name;
	@NotBlank
	String surname;
	@NotBlank
	@Email
	String email;
	@Min(value = 18)
	int age;
	@NotBlank
	String password;
	@NotBlank
	String passwordVerify;
	@NotBlank
	String phone;
	@NotBlank
	String type;


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	Integer idProfileImage;
	List<Integer> idPosts;
	List<Integer> idAttachments;
	List<Integer> idMatchedJobs;
	List<Integer> idSentMessages;
	List<Integer> idReceivedMessages;

	public String getPasswordVerify() {
		return passwordVerify;
	}

	public void setPasswordVerify(String passwordVerify) {
		this.passwordVerify = passwordVerify;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getIdProfileImage() {
		return idProfileImage;
	}

	public void setIdProfileImage(Integer idProfileImage) {
		this.idProfileImage = idProfileImage;
	}

	public List<Integer> getIdPosts() {
		return idPosts;
	}

	public void setIdPosts(List<Integer> idPosts) {
		this.idPosts = idPosts;
	}

	public List<Integer> getIdAttachments() {
		return idAttachments;
	}

	public void setIdAttachments(List<Integer> idAttachments) {
		this.idAttachments = idAttachments;
	}

	public List<Integer> getIdMatchedJobs() {
		return idMatchedJobs;
	}

	public void setIdMatchedJobs(List<Integer> idMatchedJobs) {
		this.idMatchedJobs = idMatchedJobs;
	}

	public List<Integer> getIdSentMessages() {
		return idSentMessages;
	}

	public void setIdSentMessages(List<Integer> idSentMessages) {
		this.idSentMessages = idSentMessages;
	}

	public List<Integer> getIdReceivedMessages() {
		return idReceivedMessages;
	}

	public void setIdReceivedMessages(List<Integer> idReceivedMessages) {
		this.idReceivedMessages = idReceivedMessages;
	}

	public Integer getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
