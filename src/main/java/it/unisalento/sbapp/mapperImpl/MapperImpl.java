package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.Administrator;
import it.unisalento.sbapp.domain.Applicant;
import it.unisalento.sbapp.domain.Offeror;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.dto.AdministratorDTO;
import it.unisalento.sbapp.dto.ApplicantDTO;
import it.unisalento.sbapp.dto.OfferorDTO;
import it.unisalento.sbapp.dto.UserDTO;
import it.unisalento.sbapp.iMapper.AdminMapper;
import it.unisalento.sbapp.iMapper.ApplicantMapper;
import it.unisalento.sbapp.iMapper.IMapper;
import it.unisalento.sbapp.iMapper.OfferorMapper;
import it.unisalento.sbapp.iservice.IProfileImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MapperImpl implements IMapper {

    @Autowired
    IProfileImageService profileImageService;

    @Autowired
    OfferorMapper offerorMapper;

    @Autowired
    AdminMapper adminMapper;

    @Autowired
    ApplicantMapper applicantMapper;


    public User convertToUser(UserDTO body) {
        if(body.getType().equals("offeror")) {
            OfferorDTO offerorDto = (OfferorDTO) body;
            Offeror offeror = offerorMapper.offerorDTOtoOfferor(offerorDto);
            return offeror;

        } else if (body.getType().equals("applicant")){
            ApplicantDTO applicantDTO = (ApplicantDTO) body;
            Applicant applicant = applicantMapper.applicantDTOtoApplicant(applicantDTO);
            return applicant;

        } else if (body.getType().equals("admin")) {
            AdministratorDTO administratorDTO = (AdministratorDTO) body;
            Administrator administrator = adminMapper.adminDTOtoAdministrator(administratorDTO);
            return administrator;
        } else
            return null;

    }

    @Override
    public List<UserDTO> convertToUserDTOList(List<User> userList) {
        List<UserDTO> list = new ArrayList<UserDTO>();

        for(User user: userList){
            UserDTO userDTO = convertUserToUserDTO(user);
            list.add(userDTO);
        }
        return list;
    }

    @Override
    public UserDTO convertUserToUserDTO(User user) {
        if(user.getType().equals("offeror")){
            Offeror offeror = (Offeror) user;
            OfferorDTO offerorDTO = offerorMapper.offerorToOfferorDTO(offeror);
            if(offeror.getProfileImage() != null)
                offerorDTO.setIdProfileImage(profileImageService.getProfileImageId(offeror.getProfileImage()));
            return offerorDTO;
        }
        if(user.getType().equals("applicant")){
            Applicant applicant = (Applicant) user;
            ApplicantDTO applicantDTO = applicantMapper.applicantToApplicantDTO(applicant);
            if(applicant.getProfileImage() != null)
                applicantDTO.setIdProfileImage(profileImageService.getProfileImageId(applicant.getProfileImage()));
            return applicantDTO;
        }
        if(user.getType().equals("admin")){
            Administrator administrator = (Administrator) user;
            AdministratorDTO adminDTO = adminMapper.administratorToAdminDTO(administrator);
            if(administrator.getProfileImage() != null)
                adminDTO.setIdProfileImage(profileImageService.getProfileImageId(administrator.getProfileImage()));
            return adminDTO;
        }
        return null;
    }
    
    


}
