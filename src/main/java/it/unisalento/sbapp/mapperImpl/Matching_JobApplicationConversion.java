package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.Matching_JobApplication;
import it.unisalento.sbapp.dto.Matching_JobApplicationDTO;
import it.unisalento.sbapp.iMapper.IMatching_JobApplicationConversion;
import it.unisalento.sbapp.iMapper.IMatching_JobApplicationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Matching_JobApplicationConversion implements IMatching_JobApplicationConversion {

    @Autowired
    IMatching_JobApplicationMapper matching_jobApplicationMapper;

    public List<Matching_JobApplicationDTO> convertMatchListToMatchDTOList(List<Matching_JobApplication> matching_jobApplicationList){
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = new ArrayList<Matching_JobApplicationDTO>();
        for (Matching_JobApplication matching_jobApplication : matching_jobApplicationList){
            Matching_JobApplicationDTO matching_jobApplicationDTO = matching_jobApplicationMapper.convertMatchDomainToMatchDTO(matching_jobApplication);
            matching_jobApplicationDTOList.add(matching_jobApplicationDTO);
        }

        return  matching_jobApplicationDTOList;
    }

}
