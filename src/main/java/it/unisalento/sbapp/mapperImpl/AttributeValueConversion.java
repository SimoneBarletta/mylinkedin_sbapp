package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.AttributeValue;
import it.unisalento.sbapp.dto.AttributeValueDTO;
import it.unisalento.sbapp.iMapper.IAttributeValueConversion;
import it.unisalento.sbapp.iMapper.IAttributeValueMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AttributeValueConversion implements IAttributeValueConversion {

    @Autowired
    IAttributeValueMapper attributeValueMapper;

    @Override
    public List<AttributeValueDTO> convertAttrValueListToAttrValueDTOList(List<AttributeValue> attributeValueList) {
        List<AttributeValueDTO> attributeValueDTOList = new ArrayList<AttributeValueDTO>();
        for(AttributeValue attributeValue: attributeValueList){
            AttributeValueDTO attributeValueDTO = attributeValueMapper.convertAttributeValueToAttributeValueDTO(attributeValue);
            attributeValueDTOList.add(attributeValueDTO);
        }
        return  attributeValueDTOList;
    }
}
