package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.Post;
import it.unisalento.sbapp.dto.PostDTO;
import it.unisalento.sbapp.iMapper.IPostConversion;
import it.unisalento.sbapp.iMapper.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class PostConversion implements IPostConversion {

    @Autowired
    PostMapper postMapper;

    public List<PostDTO> convertPostListToDTOList(List<Post> postList){

        List<PostDTO> postDTOList = new ArrayList<PostDTO>();

        for (Post post: postList){
            PostDTO postDTO;
            postDTO = postMapper.postToPostDTO(post);
            postDTOList.add(postDTO);
        }
        return postDTOList;

    }

}
