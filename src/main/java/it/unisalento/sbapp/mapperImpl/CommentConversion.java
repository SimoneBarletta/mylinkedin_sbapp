package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.Comment;
import it.unisalento.sbapp.dto.CommentDTO;
import it.unisalento.sbapp.iMapper.ICommentConversion;
import it.unisalento.sbapp.iMapper.ICommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CommentConversion implements ICommentConversion {

    @Autowired
    ICommentMapper commentMapper;

    @Override
    public List<CommentDTO> convertCommentListToCommentDTOList(List<Comment> commentList) {
        List<CommentDTO> commentDTOList = new ArrayList<CommentDTO>();
        for(Comment comment : commentList){
            CommentDTO commentDTO = commentMapper.convertCommentToCommentDTO(comment);
            if(comment.getParent() != null)
                commentDTO.setParentId(comment.getParent().getId());
            if(comment.getUser() != null)
                commentDTO.setUserId(comment.getUser().getId());
            commentDTOList.add(commentDTO);
        }
        return commentDTOList;
    }
}
