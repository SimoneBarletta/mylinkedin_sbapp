package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.ProfileImage;
import it.unisalento.sbapp.dto.ProfileImageDTO;
import it.unisalento.sbapp.iMapper.IProfileImageMapper;
import it.unisalento.sbapp.iservice.IProfileImageService;
import it.unisalento.sbapp.iservice.IUserService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.codec.binary.StringUtils;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;


@Component
public class ProfileImageMapperImpl implements IProfileImageMapper {

    @Autowired
    IUserService userService;

    @Autowired
    IProfileImageService profileImageService;

    @Override
    public ProfileImage convertProfileImageDTOToProfileImage(ProfileImageDTO profileImageDTO,String filePath) throws UserNotFoundExeception {

        ProfileImage profileImage = new ProfileImage();
        profileImage.setDescription(profileImageDTO.getDescription());
        profileImage.setFile_path(filePath);
        profileImage.setUser(userService.getById(profileImageDTO.getIdUser()));
        return profileImage;
    }

    @Override
    public ProfileImageDTO convertProfileImageToDTO(ProfileImage profileImage) throws IOException {
        ProfileImageDTO profileImageDTO = new ProfileImageDTO();
        profileImageDTO.setDescription(profileImage.getDescription());
        File file = new File(profileImage.getFile_path());
        MultipartFile multipartFile = new MockMultipartFile(file.getAbsolutePath(),Files.readAllBytes(file.toPath()));
        profileImageDTO.setFile(multipartFile);
        profileImageDTO.setId(profileImage.getId());
        profileImageDTO.setIdUser(profileImage.getUser().getId());
        return profileImageDTO;
    }

    @Override
    public JSONObject convertProfileImgDTOtoJson(ProfileImageDTO profileImageDTO) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(profileImageDTO.getFile().getBytes(), false)));
        JSONObject profileImgJsonObj = new JSONObject();
        try {
            profileImgJsonObj.put("id", profileImageDTO.getId());
            profileImgJsonObj.put("description", profileImageDTO.getDescription());
            profileImgJsonObj.put("imgBytesProfileB64", sb.toString());
            profileImgJsonObj.put("idUser", profileImageDTO.getIdUser());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return profileImgJsonObj;
    }
}
