package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.Structure;
import it.unisalento.sbapp.dto.StructureDTO;
import it.unisalento.sbapp.iMapper.IStructureConversion;
import it.unisalento.sbapp.iMapper.IStructureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StructureConversion implements IStructureConversion {

    @Autowired
    IStructureMapper structureMapper;

    public List<StructureDTO> convertStructureListToStructureDTOList(List<Structure> structureList){
        List<StructureDTO> structureDTOList = new ArrayList<StructureDTO>();
        for(Structure structure: structureList){
            StructureDTO structureDTO = new StructureDTO();
            structureDTO =  structureMapper.convertStructureToStructureDTO(structure);
            structureDTOList.add(structureDTO);
        }

        return structureDTOList;
    }
}
