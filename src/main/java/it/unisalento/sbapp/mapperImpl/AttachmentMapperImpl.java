package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.Exceptions.AttributeNotFoundException;
import it.unisalento.sbapp.Exceptions.PostNotFoundException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.Attachment;
import it.unisalento.sbapp.dto.AttachmentDTO;
import it.unisalento.sbapp.iMapper.IAttachmentMapper;
import it.unisalento.sbapp.iservice.IAttributeService;
import it.unisalento.sbapp.iservice.IPostService;
import it.unisalento.sbapp.iservice.IUserService;
import lombok.SneakyThrows;
import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.codec.binary.StringUtils;
import org.json.simple.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class AttachmentMapperImpl implements IAttachmentMapper {

    @Autowired
    IUserService userService;

    @Autowired
    IPostService postService;

    @SneakyThrows
    @Override
    public Attachment convertAttachmentDTOtoAttachment(AttachmentDTO attachmentDTO, String path) throws UserNotFoundExeception, PostNotFoundException {
        Attachment attachment = new Attachment();
        attachment.setFile_path(path);
        attachment.setPost(postService.getById(attachmentDTO.getIdPost()));
        attachment.setDescription(attachmentDTO.getDescription());
        attachment.setUser(userService.getById(attachmentDTO.getIdUser()));
        return attachment;
    }


    @Override
    public AttachmentDTO convertAttachmentToDTO(Attachment attachment) throws IOException{
        AttachmentDTO attachmentDTO = new AttachmentDTO();
        attachmentDTO.setDescription(attachment.getDescription());
        attachmentDTO.setId(attachment.getId());
        attachmentDTO.setIdPost(attachment.getPost().getId());
        attachmentDTO.setIdUser(attachment.getUser().getId());
        File file = new File(attachment.getFile_path());
        MultipartFile multipartFile =  new MockMultipartFile(file.getAbsolutePath(), new FileInputStream(file));
        attachmentDTO.setFile(multipartFile);
        return attachmentDTO;
    }

    @Override
    public List<AttachmentDTO> convertListAttachmentToDTOs(List<Attachment> attachmentList) throws IOException {
        List<AttachmentDTO> attachmentDTOList = new ArrayList<>();
        for(Attachment attachment : attachmentList) {
            AttachmentDTO attachmentDTO = convertAttachmentToDTO(attachment);
            attachmentDTOList.add(attachmentDTO);
        }
        return attachmentDTOList;
    }

    @Override
    public JSONArray convertAttachmentDTOListToJson(List<AttachmentDTO> attachmentDTOList) throws IOException {
        JSONArray attachmentJsonList = new JSONArray();

        for( AttachmentDTO attachmentDTO : attachmentDTOList) {
           StringBuilder sb = new StringBuilder();
           sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(attachmentDTO.getFile().getBytes(), false)));
           JSONObject attachmentJsonObj = new JSONObject();
           try {
              attachmentJsonObj.put("id", attachmentDTO.getId());
              attachmentJsonObj.put("description", attachmentDTO.getDescription());
              attachmentJsonObj.put("attachmentB64", sb.toString());
              attachmentJsonObj.put("idUser", attachmentDTO.getIdUser());
              attachmentJsonObj.put("idPost", attachmentDTO.getIdPost());
              attachmentJsonList.add(attachmentJsonObj);
           } catch (JSONException e) {
               e.printStackTrace();
           }
       }
        return attachmentJsonList;
    }
}
