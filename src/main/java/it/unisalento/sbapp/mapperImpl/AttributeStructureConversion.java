package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.AttributeStructure;
import it.unisalento.sbapp.dto.AttributeDTO;
import it.unisalento.sbapp.dto.AttributeStructureDTO;
import it.unisalento.sbapp.iMapper.IAttributeConversion;
import it.unisalento.sbapp.iMapper.IAttributeStructureConversion;
import it.unisalento.sbapp.iMapper.IAttributeStructureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AttributeStructureConversion implements IAttributeStructureConversion {

    @Autowired
    IAttributeStructureMapper attributeStructureMapper;

    @Override
    public List<AttributeStructureDTO> convertAttrStructListToAttrStrucDTOList(List<AttributeStructure> attributeStructureList) {
        List<AttributeStructureDTO> attributeStructureDTOList = new ArrayList<AttributeStructureDTO>();
        for(AttributeStructure attributeStructure: attributeStructureList){
            AttributeStructureDTO attributeStructureDTO = attributeStructureMapper.convertAttrStructoAttrStructDTO(attributeStructure);
            attributeStructureDTOList.add(attributeStructureDTO);
        }
        return  attributeStructureDTOList;
    }
}
