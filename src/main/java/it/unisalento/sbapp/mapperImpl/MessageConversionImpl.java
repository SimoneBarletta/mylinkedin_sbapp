package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.dto.MessageDTO;
import it.unisalento.sbapp.iMapper.IMessageConversion;
import it.unisalento.sbapp.iMapper.IMessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MessageConversionImpl implements IMessageConversion {

    @Autowired
    IMessageMapper messageMapper;

    public List<MessageDTO> convertMessageListToMessageDTOList(List<Message> messageList){
        List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>();
        for(Message message : messageList){
            MessageDTO messageDTO = messageMapper.convertMessageToMessageDTO(message);
            messageDTOList.add(messageDTO);
        }
        return messageDTOList;
    }
}
