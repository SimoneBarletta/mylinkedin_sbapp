package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.Sent_Message;
import it.unisalento.sbapp.dto.Sent_MessageDTO;
import it.unisalento.sbapp.iMapper.ISent_MessageConversion;
import it.unisalento.sbapp.iMapper.ISent_MessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Sent_MessageConversionImpl implements ISent_MessageConversion {

    @Autowired
    ISent_MessageMapper sent_messageMapper;

    public List<Sent_MessageDTO> convertSentMessageListToDTOList(List<Sent_Message> sent_messageList){

        List<Sent_MessageDTO> sent_messageDTOList = new ArrayList<Sent_MessageDTO>();
        for(Sent_Message sent_message: sent_messageList){
            Sent_MessageDTO sent_messageDTO = sent_messageMapper.convertSentMessageToSentMessageDTO(sent_message);
            sent_messageDTOList.add(sent_messageDTO);
        }
        return sent_messageDTOList;

    }
}
