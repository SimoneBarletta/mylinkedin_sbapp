package it.unisalento.sbapp.mapperImpl;

import it.unisalento.sbapp.domain.Attribute;
import it.unisalento.sbapp.dto.AttributeDTO;
import it.unisalento.sbapp.iMapper.IAttributeConversion;
import it.unisalento.sbapp.iMapper.IAttributeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AttributeConversion implements IAttributeConversion {

    @Autowired
    IAttributeMapper attributeMapper;

    @Override
    public List<AttributeDTO> convertAttributeListToAttributeDTOList(List<Attribute> attributeList){
        List<AttributeDTO> attributeDTOList = new ArrayList<AttributeDTO>();
        for (Attribute attribute: attributeList){
            AttributeDTO attributeDTO = attributeMapper.convertAttributeToAttributeDTO(attribute);
            attributeDTOList.add(attributeDTO);
        }
        return attributeDTOList;
    }
}
