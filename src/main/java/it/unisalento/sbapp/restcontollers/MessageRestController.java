package it.unisalento.sbapp.restcontollers;


import it.unisalento.sbapp.Exceptions.MessageNotFoundException;
import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.domain.Sent_Message;
import it.unisalento.sbapp.dto.MessageDTO;
import it.unisalento.sbapp.iMapper.IMessageConversion;
import it.unisalento.sbapp.iMapper.IMessageMapper;
import it.unisalento.sbapp.iMapper.ISent_MessageConversion;
import it.unisalento.sbapp.iservice.IMessageService;
import it.unisalento.sbapp.iservice.ISent_MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/message")
public class MessageRestController {

    @Autowired
    IMessageService messageService;

    @Autowired
    IMessageMapper messageMapper;

    @Autowired
    ISent_MessageConversion sent_messageConversion;

    @Autowired
    ISent_MessageService sent_messageService;

    @Autowired
    IMessageConversion messageConversion;

    @PostMapping(value = "/save")
    public Object save(@RequestBody @Valid MessageDTO messageDTO){
        Message message = messageMapper.convertMessageDTOToMessage(messageDTO);
        if(message != null ){
            Message saved_message = messageService.save(message);
            return saved_message;
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);

    }

    @DeleteMapping(value = "/remove/{id}")
    public void delete(@PathVariable int id) throws MessageNotFoundException {
        messageService.delete(id);
    }


    @RequestMapping(value = "/getMessageById/{id}", method = RequestMethod.GET)
    public MessageDTO getMessageById(@PathVariable int id) throws MessageNotFoundException {
        Message message = messageService.getMessageById(id);
        MessageDTO messageDTO = messageMapper.convertMessageToMessageDTO(message);
        return messageDTO;
    }


    @RequestMapping(value = "/getMessageOfUserByType/{idUser}/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MessageDTO> getMessageOfUserByType(@PathVariable int idUser, @PathVariable  String type)  {
        List<Sent_Message> sent_messageList = sent_messageService.getAllByReceiverId(idUser);
        List<Message> messageList = messageService.filterMessagesByType(sent_messageList,type);
        List<MessageDTO> messageDTOList = messageConversion.convertMessageListToMessageDTOList(messageList);
        return messageDTOList;
    }


}
