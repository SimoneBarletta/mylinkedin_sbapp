package it.unisalento.sbapp.restcontollers;

import it.unisalento.sbapp.Exceptions.Matching_JobApplicationNotFoundException;
import it.unisalento.sbapp.domain.Matching_JobApplication;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.dto.Matching_JobApplicationDTO;
import it.unisalento.sbapp.iMapper.IMatching_JobApplicationConversion;
import it.unisalento.sbapp.iMapper.IMatching_JobApplicationMapper;
import it.unisalento.sbapp.iservice.IMatching_JobApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/matchingJob")
public class MatchingJobApplicationRestController {

    @Autowired
    IMatching_JobApplicationService iMatching_jobApplicationService;
    @Autowired
    IMatching_JobApplicationMapper matching_jobApplicationMapper;

    @Autowired
    IMatching_JobApplicationConversion matching_jobApplicationConversion;

    @PostMapping(value = "/save")
    public ResponseEntity<?> save(@RequestBody @Valid Matching_JobApplicationDTO matching_jobApplicationDTO){
        Matching_JobApplication matching_jobApplication = new Matching_JobApplication();
        matching_jobApplication = matching_jobApplicationMapper.convertMatchDTOToMatchDomain(matching_jobApplicationDTO);

        if(matching_jobApplication != null){
            iMatching_jobApplicationService.save(matching_jobApplication);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);

    }

    @PutMapping(value = "/updateStatusTrue/{idStatus}")
    public ResponseEntity<?> updateStatusTrueById(@PathVariable int idStatus) {
        iMatching_jobApplicationService.updateStatusTrue(idStatus);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/updateStatusFalse/{idStatus}")
    public ResponseEntity<?> updateStatusFalseById(@PathVariable int idStatus) {
        iMatching_jobApplicationService.updateStatusFalse(idStatus);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/getByUserId/{idUser}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Matching_JobApplicationDTO> getByUserId(@PathVariable int idUser) throws Matching_JobApplicationNotFoundException {
        List<Matching_JobApplication> matching_jobApplicationList = iMatching_jobApplicationService.getByUserId(idUser);
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = matching_jobApplicationConversion.convertMatchListToMatchDTOList(matching_jobApplicationList);
        return matching_jobApplicationDTOList;
    }

    @RequestMapping(value = "/getByApplierId/{idApplier}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Matching_JobApplicationDTO> getByApplierId(@PathVariable int idApplier) throws Matching_JobApplicationNotFoundException {
        List<Matching_JobApplication> matching_jobApplicationList = iMatching_jobApplicationService.getByApplierId(idApplier);
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = matching_jobApplicationConversion.convertMatchListToMatchDTOList(matching_jobApplicationList);
        return matching_jobApplicationDTOList;
    }

    @RequestMapping(value = "/getByPostId/{idPost}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Matching_JobApplicationDTO> getByPostId(@PathVariable int idPost) throws Matching_JobApplicationNotFoundException {
        List<Matching_JobApplication> matching_jobApplicationList = iMatching_jobApplicationService.getByPostId(idPost);
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = matching_jobApplicationConversion.convertMatchListToMatchDTOList(matching_jobApplicationList);
        return matching_jobApplicationDTOList;
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Matching_JobApplicationDTO> getAll() throws Matching_JobApplicationNotFoundException {
        List<Matching_JobApplication> matching_jobApplicationList = iMatching_jobApplicationService.getAll();
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = matching_jobApplicationConversion.convertMatchListToMatchDTOList(matching_jobApplicationList);
        return matching_jobApplicationDTOList;
    }

    @RequestMapping(value = "/getByStatusAndPostId/{status}/{idPost}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Matching_JobApplicationDTO> getByStatusAndPostId(@PathVariable Boolean status, @PathVariable int idPost ) throws Matching_JobApplicationNotFoundException {
        List<Matching_JobApplication> matching_jobApplicationList = iMatching_jobApplicationService.getByStatusAndPostId(status,idPost);
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = matching_jobApplicationConversion.convertMatchListToMatchDTOList(matching_jobApplicationList);
        return matching_jobApplicationDTOList;
    }

    @RequestMapping(value = "/getByStatusAndUserId/{status}/{idUser}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Matching_JobApplicationDTO> getByStatusAndUserId(@PathVariable Boolean status, @PathVariable int idUser ) throws Matching_JobApplicationNotFoundException {
        List<Matching_JobApplication> matching_jobApplicationList = iMatching_jobApplicationService.getByStatusAndPostId(status,idUser);
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = matching_jobApplicationConversion.convertMatchListToMatchDTOList(matching_jobApplicationList);
        return matching_jobApplicationDTOList;
    }

    @RequestMapping(value = "/getByStatusAndApplierId/{status}/{idApplier}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Matching_JobApplicationDTO> getByStatusAndApplierId(@PathVariable Boolean status, @PathVariable int idApplier ) throws Matching_JobApplicationNotFoundException {
        List<Matching_JobApplication> matching_jobApplicationList = iMatching_jobApplicationService.getByStatusAndApplierId(status,idApplier);
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = matching_jobApplicationConversion.convertMatchListToMatchDTOList(matching_jobApplicationList);
        return matching_jobApplicationDTOList;
    }

    @RequestMapping(value = "/getByApplierIdOrUserId/{idApplier}/{idUser}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Matching_JobApplicationDTO> getByApplierIdOrUserId(@PathVariable int idApplier, @PathVariable int idUser ) throws Matching_JobApplicationNotFoundException {
        List<Matching_JobApplication> matching_jobApplicationList = iMatching_jobApplicationService.getByApplierIdOrUserId(idApplier,idUser);
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = matching_jobApplicationConversion.convertMatchListToMatchDTOList(matching_jobApplicationList);
        return matching_jobApplicationDTOList;
    }

    @RequestMapping(value = "/getByApplierIdOrUserIdAndStatus/{idApplier}/{idUser}/{status}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Matching_JobApplicationDTO> getByApplierIdOrUserIdAndStatus(@PathVariable int idApplier, @PathVariable int idUser, @PathVariable Boolean status ) throws Matching_JobApplicationNotFoundException {
        List<Matching_JobApplication> matching_jobApplicationList = iMatching_jobApplicationService.getByApplierIdOrUserIdAndStatus(idApplier,idUser, status);
        List<Matching_JobApplicationDTO> matching_jobApplicationDTOList = matching_jobApplicationConversion.convertMatchListToMatchDTOList(matching_jobApplicationList);
        return matching_jobApplicationDTOList;
    }



    @RequestMapping (value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> delete(@PathVariable("id") int id) throws Matching_JobApplicationNotFoundException {

        iMatching_jobApplicationService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }



}
