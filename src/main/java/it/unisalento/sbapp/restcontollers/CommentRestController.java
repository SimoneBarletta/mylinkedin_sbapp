package it.unisalento.sbapp.restcontollers;

import it.unisalento.sbapp.Exceptions.CommentNotFoundException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.Comment;
import it.unisalento.sbapp.dto.CommentDTO;
import it.unisalento.sbapp.iMapper.ICommentConversion;
import it.unisalento.sbapp.iMapper.ICommentMapper;
import it.unisalento.sbapp.iservice.ICommentService;
import it.unisalento.sbapp.iservice.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/comment")
public class CommentRestController {

    @Autowired
    ICommentService commentService;

    @Autowired
    ICommentMapper commentMapper;

    @Autowired
    ICommentConversion commentConversion;

    @Autowired
    IUserService userService;


    @PostMapping(value = "/save")
    public ResponseEntity<?> save(@RequestBody @Valid CommentDTO commentDTO) throws CommentNotFoundException, UserNotFoundExeception {
        Comment comment = commentMapper.convertCommentDTOToComment(commentDTO);
        if(comment != null){
            if (commentDTO.getParentId() != null)
                comment.setParent(commentService.getCommentById(commentDTO.getParentId()));
            if (commentDTO.getUserId() != null)
                comment.setUser(userService.getById(commentDTO.getUserId()));
            commentService.save(comment);

            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) throws CommentNotFoundException {
        commentService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/removeByPostId/{id}")
    public ResponseEntity<?> deleteAllByPost(@PathVariable Integer id) throws CommentNotFoundException {
        commentService.deleteAllByPost(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/removeByParentId/{id}")
    public ResponseEntity<?> deleteAllByParentId(@PathVariable Integer id) throws CommentNotFoundException {
        commentService.deleteAllByParent(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/getComment/{id}")
    public CommentDTO getComment(@PathVariable Integer id) throws CommentNotFoundException {
        Comment comment = commentService.getCommentById(id);
        CommentDTO commentDTO = commentMapper.convertCommentToCommentDTO(comment);
        if(comment.getParent() != null)
            commentDTO.setParentId(comment.getParent().getId());
        if(comment.getUser() != null)
            commentDTO.setUserId(comment.getUser().getId());
        return commentDTO;
    }

    @GetMapping(value = "/getCommentByPostId/{id}")
    public List<CommentDTO> getCommentByPostId(@PathVariable Integer id) throws CommentNotFoundException {
        List<Comment> commentList = commentService.getByPostId(id);
        List<CommentDTO> commentDTOList = commentConversion.convertCommentListToCommentDTOList(commentList);
        return commentDTOList;
    }

    @GetMapping(value = "/getCommentByPostIdAndParentId/{postId}/{parentId}")
    public List<CommentDTO> getCommentByPostIdAndParentId(@PathVariable Integer postId, @PathVariable Integer parentId) throws CommentNotFoundException {
        List<Comment> commentList = commentService.getByPostIdAndParentId(postId, parentId);
        List<CommentDTO> commentDTOList = commentConversion.convertCommentListToCommentDTOList(commentList);
        return commentDTOList;
    }

    @GetMapping(value = "/getChildrenByParentId/{id}")
    public List<CommentDTO> getChildrenByParentId(@PathVariable Integer id) throws CommentNotFoundException {
        List<Comment> commentList = commentService.getChildrenByParentId(id);
        List<CommentDTO> commentDTOList = commentConversion.convertCommentListToCommentDTOList(commentList);
        return commentDTOList;
    }

}
