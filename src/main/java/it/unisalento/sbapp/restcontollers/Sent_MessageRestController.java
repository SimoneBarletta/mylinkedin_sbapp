package it.unisalento.sbapp.restcontollers;

import it.unisalento.sbapp.Exceptions.MessageNotFoundException;
import it.unisalento.sbapp.Exceptions.SavingSent_MessageException;
import it.unisalento.sbapp.domain.Message;
import it.unisalento.sbapp.domain.Sent_Message;
import it.unisalento.sbapp.dto.MessageDTO;
import it.unisalento.sbapp.dto.Sent_MessageDTO;
import it.unisalento.sbapp.iMapper.ISent_MessageConversion;
import it.unisalento.sbapp.iMapper.ISent_MessageMapper;
import it.unisalento.sbapp.iservice.IMessageService;
import it.unisalento.sbapp.iservice.ISent_MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/sentMessage")
public class Sent_MessageRestController {

    @Autowired
    ISent_MessageConversion sent_messageConversion;

    @Autowired
    ISent_MessageMapper sent_messageMapper;

    @Autowired
    ISent_MessageService sent_messageService;

    @Autowired
    IMessageService messageService;


    @PostMapping(value = "/save")

    public ResponseEntity<?> save(@RequestBody @Valid Sent_MessageDTO sentMessageDTO) throws SavingSent_MessageException {

        Sent_Message sent_message = sent_messageMapper.convertSentMessageDTOToSentMessage(sentMessageDTO);
        if (sent_message != null) {
            sent_messageService.save(sent_message);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }


    @RequestMapping(value = "/getSentMessageById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Sent_MessageDTO getSentMessageById(@PathVariable int id)  {
        Sent_Message sent_message = sent_messageService.getById(id);
        Sent_MessageDTO sent_messageDTO = sent_messageMapper.convertSentMessageToSentMessageDTO(sent_message);
        return sent_messageDTO;
    }

    @RequestMapping(value = "/getSentMessageByMessageId/{messageId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Sent_MessageDTO getSentMessageByMessageId(@PathVariable int messageId) throws MessageNotFoundException {
        Message message = messageService.getMessageById(messageId);
        Sent_Message sent_message = sent_messageService.findByMessageId(message);
        Sent_MessageDTO sent_messageDTO = sent_messageMapper.convertSentMessageToSentMessageDTO(sent_message);
        return sent_messageDTO;
    }

   @RequestMapping(value = "/getSentMessageByIdSender/{idSender}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Sent_MessageDTO> getSentMessageByIdSender(@PathVariable int idSender)  {
        List<Sent_Message> sent_messageList = sent_messageService.getAllBySenderId(idSender);
       List<Sent_MessageDTO> sent_messageDTOList = sent_messageConversion.convertSentMessageListToDTOList(sent_messageList);
        return sent_messageDTOList;
    }


    @RequestMapping(value = "/getSentMessageByIdReceiver/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Sent_MessageDTO> getSentMessageByIdReceiver(@PathVariable int id)  {
        List<Sent_Message> sent_messageList = sent_messageService.getAllByReceiverId(id);
        List<Sent_MessageDTO> sent_messageDTOList = sent_messageConversion.convertSentMessageListToDTOList(sent_messageList);
        return sent_messageDTOList;
    }

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<?> delete(@PathVariable int id)  {
        sent_messageService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
