package it.unisalento.sbapp.restcontollers;

import it.unisalento.sbapp.Exceptions.AttributeNotFoundException;
import it.unisalento.sbapp.Exceptions.SavingAttributeException;
import it.unisalento.sbapp.domain.Attribute;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.dto.AttributeDTO;
import it.unisalento.sbapp.iMapper.IAttributeConversion;
import it.unisalento.sbapp.iMapper.IAttributeMapper;
import it.unisalento.sbapp.iservice.IAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value="/attribute")
public class AttributeRestController {

    @Autowired
    IAttributeService attributeService;

    @Autowired
    IAttributeMapper attributeMapper;

    @Autowired
    IAttributeConversion attributeConversion;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveAttribute(@RequestBody @Valid AttributeDTO attributeDTO) throws SavingAttributeException {


        Attribute attribute = attributeMapper.convertAttributeDTOtoAttribute(attributeDTO);
        if(attribute != null ) {
            attributeService.save(attribute);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }


    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeDTO> getAll() throws AttributeNotFoundException {
        List<Attribute> attributesList = attributeService.getAll();
        List<AttributeDTO> attributeDTOList = attributeConversion.convertAttributeListToAttributeDTOList(attributesList);
        return attributeDTOList;
    }

    @RequestMapping(value="/getByType/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeDTO> getByType(@PathVariable String type) throws AttributeNotFoundException {
        List<Attribute> attributeList = attributeService.getByType(type);
        List<AttributeDTO> attributeDTOList = attributeConversion.convertAttributeListToAttributeDTOList(attributeList);
        return attributeDTOList;

    }

    @RequestMapping(value="/getByName/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AttributeDTO getByName(@PathVariable String name) throws AttributeNotFoundException {
        Attribute attribute = attributeService.getAttributeByName(name);
        AttributeDTO attributeDTO = attributeMapper.convertAttributeToAttributeDTO(attribute);
        return attributeDTO;
    }


    @RequestMapping(value="/getById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AttributeDTO getById(@PathVariable int id) throws AttributeNotFoundException {
        Attribute attribute = attributeService.getAttributeById(id);
        AttributeDTO attributeDTO = attributeMapper.convertAttributeToAttributeDTO(attribute);
        return attributeDTO;
    }


    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping (value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> delete(@PathVariable("id") int id) throws AttributeNotFoundException {

        attributeService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value="/update", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateAttribute(@RequestBody @Valid AttributeDTO attributeDTO) throws SavingAttributeException {


        Attribute attribute = attributeMapper.convertAttributeDTOtoAttribute(attributeDTO);
        if(attribute != null ) {
            attributeService.update(attribute);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

}
