package it.unisalento.sbapp.restcontollers;

import it.unisalento.sbapp.Exceptions.ProfileImageNotFoundException;
import it.unisalento.sbapp.Exceptions.SavingProfileImgException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.ProfileImage;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.dto.ProfileImageDTO;
import it.unisalento.sbapp.dto.UserDTO;
import it.unisalento.sbapp.iMapper.IProfileImageMapper;
import it.unisalento.sbapp.iservice.IProfileImageService;
import it.unisalento.sbapp.iservice.IUserService;
import javassist.bytecode.ByteArray;
import org.apache.coyote.Response;
import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.codec.binary.StringUtils;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/profileImg")
public class ProfileImageRestController {

    String UPLOADED_FOLDER = "/Users/simonebarletta/uploaded/" ;

    @Autowired
    IProfileImageService profileImageService;

    @Autowired
    IUserService userService;

    @Autowired
    IProfileImageMapper profileImageMapper;


    @PostMapping(value="/public/upload")
    public ResponseEntity<?> upload(@ModelAttribute ProfileImageDTO profileImageDTO) throws IOException, UserNotFoundExeception, SavingProfileImgException {

        String path = saveFile(profileImageDTO.getFile());
        ProfileImage profileImage = profileImageMapper.convertProfileImageDTOToProfileImage(profileImageDTO, path);
        profileImageService.save(profileImage);
        return new ResponseEntity("Ok", HttpStatus.OK);
    }


    @PostMapping(value="/updateImgProfile/{userId}")
    public ResponseEntity<?> updateImgProfile(@PathVariable int userId, @RequestBody MultipartFile file,String description) throws IOException, UserNotFoundExeception, SavingProfileImgException {
        User user = userService.getById(userId);
        ProfileImage profileImage = user.getProfileImage();
        profileImage.setDescription(description);
        String path = saveFile(file);
        profileImage.setFile_path(path);
        profileImageService.save(profileImage);
        return new ResponseEntity("Done", HttpStatus.OK);
    }




    @RequestMapping (value = "/removeByUserId/{idUser}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("idUser") Integer idUser) throws UserNotFoundExeception, ProfileImageNotFoundException {

        profileImageService.deleteByUserId(idUser);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = "/getImgByUserId/{idUser}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONObject getByUserId(@PathVariable Integer idUser) throws UserNotFoundExeception, IOException {

        ProfileImage profileImage = profileImageService.getByUserId(idUser);
        ProfileImageDTO profileImageDTO = profileImageMapper.convertProfileImageToDTO(profileImage);
        JSONObject profileImgJsonObj = profileImageMapper.convertProfileImgDTOtoJson(profileImageDTO);
        return profileImgJsonObj;
    }



    public String saveFile(MultipartFile file) throws IOException {

        byte[] bytes = file.getBytes();
        String filename = file.getOriginalFilename();
        String file_path = UPLOADED_FOLDER + filename;
        Path path = Paths.get(UPLOADED_FOLDER + filename);
        try {
            Files.copy(file.getInputStream(), path);
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
        return file_path;
    }

    @GetMapping("/files")
    @ResponseBody
    public ResponseEntity<?> serveFile() {

        File file = new File("/Users/antoniodesantis/uploaded/wei.jpg");
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getName() + "\"").body(file);
    }

}
