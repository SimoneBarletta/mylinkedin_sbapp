package it.unisalento.sbapp.restcontollers;

import java.util.ArrayList;
import java.util.List;

import it.unisalento.sbapp.Exceptions.PostNotFoundException;
import it.unisalento.sbapp.Exceptions.SavingPostException;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.dto.UserDTO;
import it.unisalento.sbapp.iMapper.IPostConversion;
import it.unisalento.sbapp.iMapper.PostMapper;
import it.unisalento.sbapp.iservice.IStructureService;
import it.unisalento.sbapp.iservice.IUserService;
import it.unisalento.sbapp.mapperImpl.PostConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;
import it.unisalento.sbapp.domain.Post;
import it.unisalento.sbapp.dto.PostDTO;
import it.unisalento.sbapp.iservice.IPostService;

import javax.validation.Valid;

@RestController
@RequestMapping("/post")
public class PostRestController {

	@Autowired
	IPostService postService;

	@Autowired
	PostMapper postMapper;

	@Autowired
	IStructureService structureService;

	@Autowired
	IUserService userService;

	@Autowired
	IPostConversion postConversion;

	@RequestMapping(value= "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	public List<PostDTO> getAll() {

		List<Post> listPost = postService.getAll();
		List<PostDTO> postDTOList = postConversion.convertPostListToDTOList(listPost);
		return postDTOList;

	}

	@RequestMapping(value= "/getPost/{type}/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	public List<PostDTO> getPost(@PathVariable("type") String type, @PathVariable("userId") int userId ) {

		List<Post> listPost = postService.findPost(type, userId);
		List<PostDTO> postDTOList = postConversion.convertPostListToDTOList(listPost);
		return postDTOList;

	}

	@RequestMapping(value= "/getPostBySkill/{skill}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	public List<PostDTO> getPostBySkill(@PathVariable("skill") String skill) {

		List<Post> listPost = postService.findPostBySkill(skill);
		List<PostDTO> postDTOList = postConversion.convertPostListToDTOList(listPost);
		return postDTOList;

	}

	@RequestMapping(value= "/getPostByDate/{date}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	public List<PostDTO> getPostByDate(@PathVariable("date") String date) {

		List<Post> listPost = postService.findPostByDate(date);
		List<PostDTO> postDTOList = postConversion.convertPostListToDTOList(listPost);
		return postDTOList;

	}

	
	@RequestMapping(value="/getByUserId/{id}", method =RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getByUserId (@PathVariable("id") int userId) throws UserNotFoundExeception {
		List<Post> postList = postService.getPostByUserId(userId);
		List<PostDTO> postDTOList = postConversion.convertPostListToDTOList(postList);
		return postDTOList;
	}


	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Object save(@RequestBody @Valid PostDTO postDTO ) throws SavingPostException {

		Post post = postMapper.postDTOtoPost(postDTO);
		if (post != null){
			return postService.save(post).getId();
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}

	}


	@RequestMapping (value = "/remove/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<User> delete(@PathVariable("id") int id) throws PostNotFoundException {

		postService.deleteById(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(value = "/show/{id}")
	public ResponseEntity<User> showPost(@PathVariable("id") int id) throws PostNotFoundException {
		postService.showPost(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping (value = "/hide/{id}")
	public ResponseEntity<User> hidePost(@PathVariable("id") int id) throws PostNotFoundException {
		postService.hidePost(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
