package it.unisalento.sbapp.restcontollers;
import it.unisalento.sbapp.Exceptions.AttributeStructureNotFoundException;
import it.unisalento.sbapp.domain.AttributeStructure;
import it.unisalento.sbapp.domain.User;
import it.unisalento.sbapp.dto.AttributeStructureDTO;
import it.unisalento.sbapp.iMapper.IAttributeStructureConversion;
import it.unisalento.sbapp.iMapper.IAttributeStructureMapper;
import it.unisalento.sbapp.iservice.IAttributeStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/attributeStructure")
public class AttributeStructureRestController {

    @Autowired
    IAttributeStructureService attributeStructureService;

    @Autowired
    IAttributeStructureMapper attributeStructureMapper;

    @Autowired
    IAttributeStructureConversion attributeStructureConversion;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody AttributeStructureDTO attributeStructureDTO) {
        AttributeStructure attributeStructure = attributeStructureMapper.convertAttrStrucDTOtoAttrStruct(attributeStructureDTO);
        if(attributeStructure != null ) {
            attributeStructureService.save(attributeStructure);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @RequestMapping(value = "/getByStructureId/{idStructure}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeStructureDTO> getByStructureId(@PathVariable int idStructure) throws AttributeStructureNotFoundException {
        List<AttributeStructure> attributeStructuresList = attributeStructureService.getAllByStructureId(idStructure);
        List<AttributeStructureDTO> attributeStructureDTOList = attributeStructureConversion.convertAttrStructListToAttrStrucDTOList(attributeStructuresList);
        return attributeStructureDTOList;

    }

    @RequestMapping(value = "/getByAttributeId/{idAttribute}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeStructureDTO> getByAttributeId(@PathVariable int idAttribute) throws AttributeStructureNotFoundException {
        List<AttributeStructure> attributeStructuresList = attributeStructureService.getAllByAttributeId(idAttribute);
        List<AttributeStructureDTO> attributeStructureDTOList = attributeStructureConversion.convertAttrStructListToAttrStrucDTOList(attributeStructuresList);
        return attributeStructureDTOList;


    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeStructureDTO> getAll() throws AttributeStructureNotFoundException {
        List<AttributeStructure> attributeStructuresList = attributeStructureService.getAll();
        List<AttributeStructureDTO> attributeStructureDTOList = attributeStructureConversion.convertAttrStructListToAttrStrucDTOList(attributeStructuresList);
        return attributeStructureDTOList;

    }


    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping (value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> delete(@PathVariable("id") int id) throws AttributeStructureNotFoundException {

        attributeStructureService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }



}
