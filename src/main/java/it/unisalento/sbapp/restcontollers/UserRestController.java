package it.unisalento.sbapp.restcontollers;

import java.util.List;
import javax.validation.Valid;
import it.unisalento.sbapp.domain.*;
import it.unisalento.sbapp.dto.*;
import it.unisalento.sbapp.iMapper.IMapper;
import it.unisalento.sbapp.iservice.*;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import it.unisalento.sbapp.Exceptions.SavingUserException;
import it.unisalento.sbapp.Exceptions.UserNotFoundExeception;

@RestController
@RequestMapping("/user")
public class UserRestController {
	
	@Autowired
	IUserService userService;
	@Autowired
	IOfferorService offerorService;
	@Autowired
	IProfileImageService profileImageService;
	@Autowired
	IApplicantService applicantService;
	@Autowired
	IAdministratorService administratorService;
	@Autowired
	IMapper mapper;

	
	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO getById(@PathVariable int id) throws UserNotFoundExeception{

		User user = userService.getById(id);
		UserDTO userDTO = mapper.convertUserToUserDTO(user);
		return userDTO;
	}



	@RequestMapping(value = "/getByEmail/{email}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO get(@PathVariable String email) throws UserNotFoundExeception{

		User user = userService.getByEmail(email);
		UserDTO userDTO = mapper.convertUserToUserDTO(user);
		return userDTO;
	}

	@RequestMapping(value = "/getByType/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<?> getByType(@PathVariable String type){

		List<User> userList = userService.getByType(type);
		List<UserDTO> listUserDTO = mapper.convertToUserDTOList(userList);
		return listUserDTO;
	}

	
	@RequestMapping(value= "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	public List<UserDTO> getAll() {

		List<User> listUser = userService.getAll();
		List<UserDTO> list = mapper.convertToUserDTOList(listUser);
		return list;
		
	}
	
	@SneakyThrows
	@PostMapping(value="public/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Object save(@RequestBody @Valid UserDTO body ) throws SavingUserException{

		User user = mapper.convertToUser(body);
		if (user != null){
			return userService.save(user);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping (value = "/remove/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<User> delete(@PathVariable("id") int id) throws UserNotFoundExeception {

		userService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/ban/{id}")
	public ResponseEntity<User> banUser(@PathVariable("id") int id) throws UserNotFoundExeception {
		userService.banUser(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/unban/{id}")
	public ResponseEntity<User> unbanUser(@PathVariable("id") int id) throws UserNotFoundExeception {
		userService.unbanUser(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value= "/getPendingUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	public List<UserDTO> getPendingUsers() {
		List<User> userList = userService.getPendingUsers();
		List<UserDTO> userDTOList = mapper.convertToUserDTOList(userList);
		return userDTOList;
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/accept/{id}")
	public ResponseEntity<User> acceptUser(@PathVariable("id") int id) throws UserNotFoundExeception {
		userService.acceptUser(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	

}
