package it.unisalento.sbapp.restcontollers;


import it.unisalento.sbapp.Exceptions.StructureNotFoundException;
import it.unisalento.sbapp.domain.Structure;
import it.unisalento.sbapp.dto.StructureDTO;
import it.unisalento.sbapp.iMapper.IStructureConversion;
import it.unisalento.sbapp.iMapper.IStructureMapper;
import it.unisalento.sbapp.iservice.IStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/structure")
public class StructureRestController {

    @Autowired
    IStructureService iStructureService;


    @Autowired
    IStructureMapper structureMapper;

    @Autowired
    IStructureConversion structureConversion;


   @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody StructureDTO structureDTO) {
        Structure structure = structureMapper.convertStructureDTOToStructure(structureDTO);
        if (structure != null){
            iStructureService.save(structure);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        public List<StructureDTO> getAll () throws StructureNotFoundException {
        List<Structure> structureList = iStructureService.getAllStructures();
        List<StructureDTO> structureDTOList = structureConversion.convertStructureListToStructureDTOList(structureList);
        return structureDTOList;
    }


    @RequestMapping(value = "/getByTitle/{title}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public StructureDTO getByTitle (@PathVariable String title) throws StructureNotFoundException {
        Structure structure = iStructureService.getByTitle(title);
        StructureDTO structureDTO = structureMapper.convertStructureToStructureDTO(structure);
        return structureDTO;
    }



    @RequestMapping(value = "/getById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public StructureDTO getByTitle (@PathVariable int id ) throws StructureNotFoundException {
        Structure structure = iStructureService.getById(id);
        StructureDTO structureDTO = structureMapper.convertStructureToStructureDTO(structure);
        return structureDTO;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable int id) {
        iStructureService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
