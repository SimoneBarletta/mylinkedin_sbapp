package it.unisalento.sbapp.restcontollers;

import it.unisalento.sbapp.Exceptions.*;
import it.unisalento.sbapp.domain.Attachment;
import it.unisalento.sbapp.dto.AttachmentDTO;
import it.unisalento.sbapp.iMapper.IAttachmentMapper;
import it.unisalento.sbapp.iservice.IAttachmentService;
import it.unisalento.sbapp.iservice.IUserService;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/attachment")
public class AttachmentRestController {

    String UPLOADED_FOLDER = "/Users/simonebarletta/uploaded/" ;
    @Autowired
    IAttachmentService attachmentService;

    @Autowired
    IAttachmentMapper attachmentMapper;

    @Autowired
    IUserService userService;


    @PostMapping(value="/public/uploadAttachment")
    public ResponseEntity<?> uploadAttachment(@ModelAttribute AttachmentDTO attachmentDto) throws IOException, UserNotFoundExeception, SavingUserException, AttributeNotFoundException, PostNotFoundException {
        String path = saveFile(attachmentDto.getFile());
        Attachment attachment = attachmentMapper.convertAttachmentDTOtoAttachment(attachmentDto,path);
        attachmentService.save(attachment);
        return new ResponseEntity("Done", HttpStatus.OK);
    }

    @PostMapping(value="/updateAttachment/{id}")
    public ResponseEntity<?> updateAttachment(@PathVariable int id, @RequestBody MultipartFile file,String description) throws IOException, UserNotFoundExeception, SavingUserException {
       Attachment attachment = attachmentService.getById(id);
       attachment.setDescription(description);
       String path = saveFile(file);
       attachment.setFile_path(path);
       attachmentService.save(attachment);
       return new ResponseEntity("Ok", HttpStatus.OK);
    }

    @RequestMapping (value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") Integer id) throws AttachmentNotFoundException {
        attachmentService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = "/getAttachmentsByUserId/{idUser}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttachmentDTO> getByUserId(@PathVariable Integer idUser) throws UserNotFoundExeception, IOException {
        List<Attachment> attachmentList = attachmentService.getAttachmentByUserId(idUser);
        List<AttachmentDTO> attachmentDTOList = attachmentMapper.convertListAttachmentToDTOs(attachmentList);
        JSONArray attachmentJsonList = attachmentMapper.convertAttachmentDTOListToJson(attachmentDTOList);
        return attachmentJsonList;
    }

    @RequestMapping(value = "/public/getAttachmentsByPostId/{postId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttachmentDTO> getByAttributeId(@PathVariable Integer postId) throws IOException, PostNotFoundException {
        List<Attachment> attachmentList = attachmentService.getAttachmentByPostId(postId);
        List<AttachmentDTO> attachmentDTOList = attachmentMapper.convertListAttachmentToDTOs(attachmentList);
        JSONArray attachmentJsonList = attachmentMapper.convertAttachmentDTOListToJson(attachmentDTOList);
        return attachmentJsonList;
    }

    @RequestMapping(value = "/public/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttachmentDTO> getAll() throws IOException, PostNotFoundException {
        List<Attachment> attachmentList = attachmentService.getAll();
        List<AttachmentDTO> attachmentDTOList = attachmentMapper.convertListAttachmentToDTOs(attachmentList);
        JSONArray attachmentJsonList = attachmentMapper.convertAttachmentDTOListToJson(attachmentDTOList);
        return attachmentJsonList;
    }

    public String saveFile(MultipartFile file) throws IOException {
        byte[] bytes = file.getBytes();
        String filename = file.getOriginalFilename();
        String file_path = UPLOADED_FOLDER + filename;
        Path path = Paths.get(UPLOADED_FOLDER + filename);
        try {
            Files.copy(file.getInputStream(), path);
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
        return file_path;
    }
}
