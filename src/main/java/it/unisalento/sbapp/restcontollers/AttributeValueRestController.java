package it.unisalento.sbapp.restcontollers;

import it.unisalento.sbapp.Exceptions.AttributeValueNotFoundException;
import it.unisalento.sbapp.domain.AttributeValue;
import it.unisalento.sbapp.dto.AttributeValueDTO;
import it.unisalento.sbapp.iMapper.IAttributeValueConversion;
import it.unisalento.sbapp.iMapper.IAttributeValueMapper;
import it.unisalento.sbapp.iservice.IAttributeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/attributeValue")
public class AttributeValueRestController {

    @Autowired
    IAttributeValueService attributeValueService;

    @Autowired
    IAttributeValueMapper attributeValueMapper;

    @Autowired
    IAttributeValueConversion attributeValueConversion;


    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody AttributeValueDTO attributeValueDTO) {
        AttributeValue attributeValue = attributeValueMapper.convertAttributeValueDTOtoAttributeValue(attributeValueDTO);
        if(attributeValue != null ) {
            attributeValueService.save(attributeValue);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }


    @RequestMapping(value = "/getByAttributeName/{attribute_name}/{postId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeValueDTO> getByAttributeName(@PathVariable String attribute_name, @PathVariable int postId) throws AttributeValueNotFoundException {
        List<AttributeValue> attributeValueList = attributeValueService.findAttributeByName(attribute_name, postId);
        List<AttributeValueDTO> attributeValueDTOList = attributeValueConversion.convertAttrValueListToAttrValueDTOList(attributeValueList);
        return attributeValueDTOList;

    }

    @RequestMapping(value = "/getByAttributeId/{idAttribute}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeValueDTO> getByAttributeId(@PathVariable int idAttribute) throws AttributeValueNotFoundException {
        List<AttributeValue> attributeValueList = attributeValueService.getAllByAttributeId(idAttribute);
        List<AttributeValueDTO> attributeValueDTOList = attributeValueConversion.convertAttrValueListToAttrValueDTOList(attributeValueList);
        return attributeValueDTOList;

    }

    @RequestMapping(value = "/getByPostId/{idPost}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeValueDTO> getByPostId(@PathVariable int idPost) throws AttributeValueNotFoundException {
        List<AttributeValue> attributeValueList = attributeValueService.getAllByPostId(idPost);
        List<AttributeValueDTO> attributeValueDTOList = attributeValueConversion.convertAttrValueListToAttrValueDTOList(attributeValueList);
        return attributeValueDTOList;


    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeValueDTO> getAll() throws AttributeValueNotFoundException {
        List<AttributeValue> attributeValueList = attributeValueService.getAll();
        List<AttributeValueDTO> attributeValueDTOList = attributeValueConversion.convertAttrValueListToAttrValueDTOList(attributeValueList);
        return attributeValueDTOList;

    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping (value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<AttributeValue> delete(@PathVariable("id") int id) throws AttributeValueNotFoundException {
        attributeValueService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }


}
